package com.sinergiadigital.taxsi.pasajero.Implementation;

import android.support.v7.app.AppCompatActivity;

import com.sinergiadigital.taxsi.pasajero.Background.BaseWSManager;
import com.sinergiadigital.taxsi.pasajero.Background.WSBaseResponseInterface;
import com.sinergiadigital.taxsi.pasajero.Background.WSCallback;
import com.sinergiadigital.taxsi.pasajero.BaseClasses.BasePresenter;
import com.sinergiadigital.taxsi.pasajero.R;
import com.sinergiadigital.taxsi.pasajero.Utils.MessageUtils;

public abstract class TaxsiPresenter extends BasePresenter implements WSCallback {
    protected BaseWSManager mWSManager;


    @Override
    public void onResume() {
        super.onResume();
    }

    public TaxsiPresenter(AppCompatActivity appCompatActivity) {
        super(appCompatActivity);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mWSManager = initWSManager();
    }

    public abstract BaseWSManager initWSManager();

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mWSManager != null) mWSManager.onDestroy();
    }

    @Override
    public void onRequestWS(String requestUrl) {
        MessageUtils.progress(mAppCompatActivity, R.string.dialog_loading);
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        MessageUtils.stopProgress();
    }

    @Override
    public void onErrorLoadResponse(String requestUrl, String messageError) {
        MessageUtils.stopProgress();
        if (messageError.trim().equals("")) {
            MessageUtils.toast(mContext, R.string.dialog_intern_error);
        } else {
            MessageUtils.toast(mContext, messageError);
        }
    }

    @Override
    public void onErrorConnection() {
        MessageUtils.stopProgress();
        MessageUtils.toast(mContext, R.string.dialog_error_connection);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

}

