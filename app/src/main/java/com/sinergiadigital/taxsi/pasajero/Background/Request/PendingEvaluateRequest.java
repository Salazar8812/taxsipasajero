package com.sinergiadigital.taxsi.pasajero.Background.Request;

import com.sinergiadigital.taxsi.pasajero.Background.WSBaseRequestInterface;

public class PendingEvaluateRequest implements WSBaseRequestInterface {
    public String id_pasajero;

    public PendingEvaluateRequest(String id_pasajero) {
        this.id_pasajero = id_pasajero;
    }
}
