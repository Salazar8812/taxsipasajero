package com.sinergiadigital.taxsi.pasajero.Model;

import com.google.gson.annotations.SerializedName;

public class Travel {
    @SerializedName("estatus")
    public String estatus;

    @SerializedName("id")
    public String id;

    @SerializedName("latitud")
    public String latitud;

    @SerializedName("longitud")
    public String longitud;

    @SerializedName("angulo")
    public String angulo;

}
