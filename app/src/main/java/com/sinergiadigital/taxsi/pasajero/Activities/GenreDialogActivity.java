package com.sinergiadigital.taxsi.pasajero.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Window;

import com.sinergiadigital.taxsi.pasajero.Adapters.GenreAdapter;
import com.sinergiadigital.taxsi.pasajero.BaseClasses.BaseActivity;
import com.sinergiadigital.taxsi.pasajero.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GenreDialogActivity extends BaseActivity implements GenreAdapter.GenreCallback {
    @BindView(R.id.mListGenre)
    RecyclerView mListGenre;
    private GenreAdapter mGenreAdapter;
    private List<String> mList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.genre_dialog_activity);
        ButterKnife.bind(this);
        populate();
        configureRecycler();
    }

    private void populate(){
        mList.add("Hombre");
        mList.add("Mujer");
    }

    private void configureRecycler(){
        mListGenre.setLayoutManager(new LinearLayoutManager(this));
        mGenreAdapter = new GenreAdapter(this);
        mListGenre.setAdapter(mGenreAdapter);
        mGenreAdapter.update(mList);
    }

    @Override
    public void OnGetGenre(String mGenre) {
        Intent intent=new Intent();
        intent.putExtra("RESULT_STRING", mGenre);
        setResult(RESULT_OK, intent);
        finish();
    }
}
