package com.sinergiadigital.taxsi.pasajero.Adapters;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.sinergiadigital.taxsi.pasajero.Background.Response.PredictionsResponse;
import com.sinergiadigital.taxsi.pasajero.R;

public class AutoCompleteHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
    TextView mTitleTextView;
    TextView mDescriptionTextView;
    private AutoCompleteAdapter.ItemCallback mItemCallback;
    private PredictionsResponse.PredictionsEntity mPredictionsEntity;
    private String mType;


    public AutoCompleteHolder(View itemView) {
        super(itemView);
        mTitleTextView = itemView.findViewById(R.id.mTitleTextView);
        mDescriptionTextView = itemView.findViewById(R.id.mDescriptionTextView);
        itemView.setOnClickListener(this);
    }

    public void render(@NonNull PredictionsResponse.PredictionsEntity predictionsEntity, @NonNull Boolean selection, AutoCompleteAdapter.ItemCallback itemCallback, String mType) {
        this.mPredictionsEntity = predictionsEntity;
        mItemCallback = itemCallback;
        this.mType = mType;
        mTitleTextView.setText(predictionsEntity.description);
        mDescriptionTextView.setText(predictionsEntity.structuredFormatting.secondary_text);
    }

    @Override
    public void onClick(View view) {
        mItemCallback.OnGetItemClick(this,mType);
    }

    public PredictionsResponse.PredictionsEntity getPrediction() {
        return mPredictionsEntity;
    }
}
