package com.sinergiadigital.taxsi.pasajero.Utils;

public class UpperCaseCharacter {

    public String UpperCaseCharacter(String mCad) {
        return mCad.substring(0,1).toUpperCase() + mCad.substring(1);
    }
}
