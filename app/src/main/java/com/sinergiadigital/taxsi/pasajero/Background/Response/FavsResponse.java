package com.sinergiadigital.taxsi.pasajero.Background.Response;

import com.google.gson.annotations.SerializedName;
import com.sinergiadigital.taxsi.pasajero.Background.WSBaseResponseInterface;

import java.util.ArrayList;
import java.util.List;

public class FavsResponse implements WSBaseResponseInterface {
    @SerializedName("resultado")
    public List<PendingEvaluateResponse.Resultado> resultado = new ArrayList<>();

    @SerializedName("lugar")
    public List<Lugares> mListPlace = new ArrayList<>();

}
