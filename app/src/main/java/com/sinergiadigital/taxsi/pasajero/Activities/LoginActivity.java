package com.sinergiadigital.taxsi.pasajero.Activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.sinergiadigital.taxsi.pasajero.BaseClasses.BaseActivity;
import com.sinergiadigital.taxsi.pasajero.BaseClasses.BasePresenter;
import com.sinergiadigital.taxsi.pasajero.GPSService.GPSClass;
import com.sinergiadigital.taxsi.pasajero.Implementation.LoginPresenter;
import com.sinergiadigital.taxsi.pasajero.Implementation.RegisterPresenter;
import com.sinergiadigital.taxsi.pasajero.InternalData.PrefsTaxsi;
import com.sinergiadigital.taxsi.pasajero.R;
import com.sinergiadigital.taxsi.pasajero.Utils.EditTextValidator;
import com.sinergiadigital.taxsi.pasajero.Utils.FormValidator;
import com.sinergiadigital.taxsi.pasajero.Utils.Regex;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends BaseActivity implements FacebookCallback<LoginResult>, GraphRequest.GraphJSONObjectCallback,RegisterPresenter.RegisterUserCallback {

    @BindView(R.id.mEmailEditText)
    public EditText mEmailEditText;
    @BindView(R.id.mPasswordEditText)
    public EditText mPasswordEditText;
    @BindView(R.id.mBackgroundImageView)
    public ImageView mBackgroundImageView;
    @BindView(R.id.mLogInFBButton)
    public LoginButton mLogInFBButton;
    private LocationManager locationManager;
    private PrefsTaxsi mPrefsTaxsi;
    private LoginPresenter mLoginPresenter;
    private CallbackManager callbackManager;
    private RegisterPresenter registerPresenter;
    private String mName="",mLastname="",mId = "", mEmail ="",mBirthday = "";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.login_activity);
        ButterKnife.bind(this);

        mPrefsTaxsi = new PrefsTaxsi(this);

        configureLoginFacebook();

        setFieldValidator();

        Glide.with(this).load(R.drawable.ic_login_bakcground).into(mBackgroundImageView);

        requestPermission();

        generateHash();
    }


    public void generateHash(){
        try {
            PackageInfo info =  getPackageManager().getPackageInfo("com.sinergiadigital.taxsi.pasajero",  PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String sign=Base64.encodeToString(md.digest(), Base64.DEFAULT);
                Log.e("MY KEY HASH:", sign);
                //Toast.makeText(getApplicationContext(),sign,  Toast.LENGTH_LONG).show();
            }
        } catch (PackageManager.NameNotFoundException e) {
        } catch (NoSuchAlgorithmException e) {
        }
    }
    public void configureLoginFacebook(){
        callbackManager = CallbackManager.Factory.create();
        mLogInFBButton.setReadPermissions("email");
        mLogInFBButton.registerCallback(callbackManager, this);
    }

    public void setFieldValidator(){
        /*mEmailEditText.setText("paco2057@hotmail.com");
        mPasswordEditText.setText("1234");*/

        mFormValidator = new FormValidator(this);
        mFormValidator.addValidators(
                new EditTextValidator(mEmailEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mPasswordEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty)
        );
    }

    @Override
    protected BasePresenter[] getPresenters(){
        mLoginPresenter = new LoginPresenter(this);
        registerPresenter = new RegisterPresenter(this,this);
        return new BasePresenter[]{mLoginPresenter,registerPresenter};
    }

    private void initServiceGPS(){
        Intent i =new Intent(getApplicationContext(),GPSClass.class);
        startService(i);
    }

    @OnClick(R.id.mLoginButton)
    public void OnClickLogin(){
        if(mFormValidator.isValid()) {
            View view = this.getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
            mLoginPresenter.doLogin(mEmailEditText.getText().toString().trim(), mPasswordEditText.getText().toString().trim());
        }
    }

    @OnClick(R.id.mCreateAccoutButton)
    public void OnClickCreateAccount(){
        startActivity(new Intent(this,RegisterActivity.class));
    }


    private void requestPermission(){
        if (ActivityCompat.checkSelfPermission(LoginActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(LoginActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions(LoginActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 1); //Any number can be used    }
        }else{
            gpsCheckStatus();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    gpsCheckStatus();
                }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
        starLocation();
    }

    public void gpsCheckStatus(){
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        boolean enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (!enabled) {
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivityForResult(intent,0);
        }else{
            starLocation();
        }
    }

    public void starLocation(){
        LocalBroadcastManager.getInstance(this).registerReceiver(
                new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        String latitude = intent.getStringExtra(GPSClass.EXTRA_LATITUDE);
                        String longitude = intent.getStringExtra(GPSClass.EXTRA_LONGITUDE);
                        Log.e("Latitude", latitude);
                        Log.e("Longitud", longitude);
                        mPrefsTaxsi.saveData("latitude",latitude);
                        mPrefsTaxsi.saveData("longitude",longitude);
                    }
                }, new IntentFilter(GPSClass.ACTION_LOCATION_BROADCAST)
        );

        initServiceGPS();
    }

    @Override
    public void onSuccess(LoginResult loginResult) {
        LoginManager.getInstance().logOut();
        GraphRequest.newMeRequest(loginResult.getAccessToken(), this).executeAsync();
    }

    @Override
    public void onCancel() {
        Log.e("Error", "Ocurrio un error");
    }

    @Override
    public void onError(FacebookException e) {
        Log.e("Error",e.toString());
    }

    public void register(){

        if(mEmail.equals("")){
            registerPresenter.registerUser("0",mName,
                    mLastname,
                    mBirthday,
                    "",
                    mId,
                    "",
                    mId,
                    "https://graph.facebook.com/"+mId+"/picture?type=large");
        }else{
            registerPresenter.registerUser("0",mName,
                    mLastname,
                    mBirthday,
                    "",
                    mEmail,
                    "",
                    mId,
                    "https://graph.facebook.com/"+mId+"/picture?type=large");
        }

    }

    @Override
    public void onCompleted(JSONObject jsonObject, GraphResponse graphResponse) {
        mBirthday = getElement(jsonObject, "birthday");
        mEmail = getElement(jsonObject,"email");
        mId = getElement(jsonObject,"id");
        mName = getElement(jsonObject,"name");
        mLastname = getElement(jsonObject, "last_name");
        register();
    }

    public String getElement(JSONObject mJsonObject, String key){
        String mElement = "";
        try{
            if (mJsonObject.has(key)) {
                mElement = String.valueOf(mJsonObject.getString(key));
                Log.e(key, String.valueOf(mJsonObject.getString(key)));
            }
        }catch (NullPointerException | IllegalArgumentException  e){
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return mElement;
    }

    @Override
    public void OnSuccessRegister() {
        startActivity(new Intent(this,MainMapActivity.class));
    }
}
