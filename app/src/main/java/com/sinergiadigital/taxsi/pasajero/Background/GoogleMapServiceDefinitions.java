package com.sinergiadigital.taxsi.pasajero.Background;

import com.sinergiadigital.taxsi.pasajero.Background.Response.PredictionsResponse;
import com.sinergiadigital.taxsi.pasajero.Model.ResultMaps;
import com.sinergiadigital.taxsi.pasajero.Model.StimulatedTimeMaps;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GoogleMapServiceDefinitions {
    @GET("/maps/api/geocode/json")
    Call<ResultMaps> fetchLocation(@Query("latlng") String latlng, @Query("sensor") String sensor);

    @GET("/maps/api/place/autocomplete/json")
    Call<ResponseBody> autoComplete(@Query("input") String input,
                                           /*@Query("location") String location,
                                           @Query("radius") String radius,
                                           @Query("language") String language,*/
                                           @Query("key") String key);

    @GET("/maps/api/place/details/json")
    Call<ResponseBody> getDetailPlace(@Query("placeid") String placeId , @Query("key") String key);


    @GET("/maps/api/geocode/json")
    Call<ResultMaps> fetchDirectionFromText(@Query("address") String address);

    @GET("/maps/api/directions/json")
    Call<StimulatedTimeMaps> fetchDirection(@Query("origin") String origin, @Query("destination") String destination, @Query("sensor") String sensor);


}
