package com.sinergiadigital.taxsi.pasajero.Model;

import com.google.gson.annotations.SerializedName;

public class Geometry {
    @SerializedName("location")
    public LocationPlace locationPlace;
}
