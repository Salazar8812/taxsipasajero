package com.sinergiadigital.taxsi.pasajero.Background.Response;

import com.google.gson.annotations.SerializedName;
import com.sinergiadigital.taxsi.pasajero.Background.WSBaseResponseInterface;
import com.sinergiadigital.taxsi.pasajero.Model.ResultDetail;

public class DetailPlaceResponse implements WSBaseResponseInterface {
   @SerializedName("result")
    public ResultDetail resultDetail;
}
