package com.sinergiadigital.taxsi.pasajero.Implementation;

import android.support.v7.app.AppCompatActivity;

import com.sinergiadigital.taxsi.pasajero.Background.BaseWSManager;
import com.sinergiadigital.taxsi.pasajero.Background.Request.TakeTripRequest;
import com.sinergiadigital.taxsi.pasajero.Background.Response.FavsResponse;
import com.sinergiadigital.taxsi.pasajero.Background.Response.Lugares;
import com.sinergiadigital.taxsi.pasajero.Background.WSBaseResponseInterface;
import com.sinergiadigital.taxsi.pasajero.Background.WSManager;
import com.sinergiadigital.taxsi.pasajero.InternalData.PrefsTaxsi;
import com.sinergiadigital.taxsi.pasajero.Utils.MessageUtils;

import java.util.List;

public class FavsPresenter extends TaxsiPresenter {
    private PrefsTaxsi mPrefsTaxsi;
    private FavsCallback mFavsCallback;

    public interface FavsCallback{
        void OnSuccessGetFavs(List<Lugares> mListLugares);
    }

    public FavsPresenter(AppCompatActivity appCompatActivity, FavsCallback mFavsCallback) {
        super(appCompatActivity);
        mPrefsTaxsi = new PrefsTaxsi(mAppCompatActivity);
        this.mFavsCallback = mFavsCallback;
    }

    @Override
    public BaseWSManager initWSManager() {
        return mWSManager = WSManager.init().settings(mContext, this);
    }

    public void getFavs(){
        TakeTripRequest takeTripRequest = new TakeTripRequest(mPrefsTaxsi.getData("id_pasajero"));
        mWSManager.requestWs(FavsResponse.class, WSManager.WS.FAVORITES, takeTripRequest);
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        if(requestUrl.equals(WSManager.WS.FAVORITES)){
            OnSuccessFavs((FavsResponse) baseResponse);
        }
    }

    public void OnSuccessFavs(FavsResponse favsResponse){
        MessageUtils.stopProgress();
        if(favsResponse != null){
            if(!favsResponse.resultado.get(0).resultado.equals("0")){
                mFavsCallback.OnSuccessGetFavs(favsResponse.mListPlace);
            }
        }
    }
}
