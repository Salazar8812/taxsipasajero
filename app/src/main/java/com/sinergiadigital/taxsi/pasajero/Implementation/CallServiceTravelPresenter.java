package com.sinergiadigital.taxsi.pasajero.Implementation;

import android.support.v7.app.AppCompatActivity;
import com.sinergiadigital.taxsi.pasajero.Background.BaseWSManager;
import com.sinergiadigital.taxsi.pasajero.Background.Request.CalculatePriceRequest;
import com.sinergiadigital.taxsi.pasajero.Background.Request.CallServiceTaxsiRequest;
import com.sinergiadigital.taxsi.pasajero.Background.Request.CoordinatesDriverRequest;
import com.sinergiadigital.taxsi.pasajero.Background.Request.PendingEvaluateRequest;
import com.sinergiadigital.taxsi.pasajero.Background.Response.CalculatePriceResponse;
import com.sinergiadigital.taxsi.pasajero.Background.Response.CallServiceTaxsiResponse;
import com.sinergiadigital.taxsi.pasajero.Background.Response.CoordinatesDriverResponse;
import com.sinergiadigital.taxsi.pasajero.Background.Response.PendingEvaluateResponse;
import com.sinergiadigital.taxsi.pasajero.Background.WSBaseResponseInterface;
import com.sinergiadigital.taxsi.pasajero.Background.WSManager;
import com.sinergiadigital.taxsi.pasajero.InternalData.PrefsTaxsi;
import com.sinergiadigital.taxsi.pasajero.Utils.MessageUtils;

public class CallServiceTravelPresenter extends TaxsiPresenter {
    public interface ServiceTaxsiCallback{
        void OnSuccessGetTarifa(CalculatePriceResponse calculatePriceResponse);

        void OnSuccessCallServiceTaxsi();
    }

    private PrefsTaxsi mPrefsTaxe;
    private ServiceTaxsiCallback mServiceTaxsiCallback;


    @Override
    public BaseWSManager initWSManager() {
        return mWSManager = WSManager.init().settings(mContext, this);
    }


    @Override
    public void onRequestWS(String requestUrl) {
        super.onRequestWS(requestUrl);
        MessageUtils.stopProgress();
    }

    public CallServiceTravelPresenter(AppCompatActivity appCompatActivity, ServiceTaxsiCallback mServiceTaxsiCallback) {
        super(appCompatActivity);
        this.mServiceTaxsiCallback = mServiceTaxsiCallback;
    }

    public void callServiceTasxi(String lugar_partida, String lugar_llegada, String latitudOrigen, String longitudOrigen, String latitudDestino, String longitudDestino){
        MessageUtils.progress(mAppCompatActivity,"Cargando información");
        mPrefsTaxe = new PrefsTaxsi(mAppCompatActivity);
        CallServiceTaxsiRequest callServiceTaxsiRequest = new CallServiceTaxsiRequest(mPrefsTaxe.getData("id_pasajero"),
                lugar_partida,
                lugar_llegada,
                latitudOrigen,
                longitudOrigen,
                latitudDestino,
                longitudDestino);
        mWSManager.requestWs(CallServiceTaxsiResponse.class, WSManager.WS.CALL_SERVICE_TAXSI, callServiceTaxsiRequest);
    }

    public void calculatePrice(String distancia,String tiempo){
        CalculatePriceRequest calcularTarifaRequest = new CalculatePriceRequest(distancia,tiempo);
        mWSManager.requestWs(CalculatePriceResponse.class, WSManager.WS.CALCULAR_TARIFA, calcularTarifaRequest);
    }

    public void getCoordinatesTravel(){
        CoordinatesDriverRequest coordinatesDriverRequest = new CoordinatesDriverRequest(mPrefsTaxe.getData("id_viaje"));
        mWSManager.requestWs(CoordinatesDriverResponse.class, WSManager.WS.GET_COORDINATES_DRIVER, coordinatesDriverRequest);
    }

    public void getPendingEvaluate(){
        PendingEvaluateRequest pendingEvaluateRequest = new PendingEvaluateRequest(mPrefsTaxe.getData("id_pasajero"));
        mWSManager.requestWs(PendingEvaluateResponse.class, WSManager.WS.GET_PENDING_TRAVEL,pendingEvaluateRequest);
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        switch (requestUrl){
            case WSManager.WS.CALL_SERVICE_TAXSI:
                OnSuccessCallServiceTaxsi((CallServiceTaxsiResponse) baseResponse);
                break;
            case WSManager.WS.CALCULAR_TARIFA:
                OnSuccessCalculatePrice((CalculatePriceResponse) baseResponse);
                break;

            case WSManager.WS.GET_COORDINATES_DRIVER:
                OnSuccessGetCoordinatesDriver((CoordinatesDriverResponse) baseResponse);
                break;
        }
    }

    public void OnSuccessCallServiceTaxsi(CallServiceTaxsiResponse callServiceTaxsiResponse){
        MessageUtils.stopProgress();
        if(callServiceTaxsiResponse != null){
            if(callServiceTaxsiResponse.resultadoServiceCall.equals("1")){
                mPrefsTaxe.saveData("id_viaje",callServiceTaxsiResponse.resultadoServiceCall.get(0).id_viaje);
                mPrefsTaxe.saveData("id_chofer",callServiceTaxsiResponse.resultadoServiceCall.get(0).id_conductor);
                mPrefsTaxe.saveData("status_viaje",callServiceTaxsiResponse.resultadoServiceCall.get(0).estatus);
            }
        }
    }

    public void OnSuccessCalculatePrice(CalculatePriceResponse calcularTarifaResponse){
        MessageUtils.stopProgress();
        try{
            if(!calcularTarifaResponse.tarifa.get(0).tarifa.equals("")) {
                mServiceTaxsiCallback.OnSuccessGetTarifa(calcularTarifaResponse);
            }else{
                MessageUtils.toast(mAppCompatActivity, "Ocurrio un error al calcular tarifa");
            }
        }catch (NullPointerException e){
            e.printStackTrace();
            MessageUtils.toast(mAppCompatActivity, "Ocurrio un error al calcular tarifa");
        }
    }

    public void OnSuccessGetCoordinatesDriver(CoordinatesDriverResponse coordinatesDriverResponse){
        MessageUtils.stopProgress();
        if(coordinatesDriverResponse != null){
            if(coordinatesDriverResponse.resultadoServiceCall.resultado.equals("1")){

            }
        }
    }

}
