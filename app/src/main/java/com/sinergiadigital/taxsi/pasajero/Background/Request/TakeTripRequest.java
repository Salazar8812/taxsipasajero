package com.sinergiadigital.taxsi.pasajero.Background.Request;

import com.google.gson.annotations.SerializedName;
import com.sinergiadigital.taxsi.pasajero.Background.WSBaseRequestInterface;

public class TakeTripRequest implements WSBaseRequestInterface {
    @SerializedName("id_conductor")
    public String id_conductor;

    @SerializedName("id_viaje")
    public String id_viaje;

    public TakeTripRequest(String id_conductor, String id_viaje) {
        this.id_conductor = id_conductor;
        this.id_viaje = id_viaje;
    }

    public TakeTripRequest(String id_conductor) {
        this.id_conductor = id_conductor;
    }
}
