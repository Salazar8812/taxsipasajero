package com.sinergiadigital.taxsi.pasajero.Activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.sinergiadigital.taxsi.pasajero.BaseClasses.BaseActivity;
import com.sinergiadigital.taxsi.pasajero.BaseClasses.BasePresenter;
import com.sinergiadigital.taxsi.pasajero.Implementation.EvaluatePresenter;
import com.sinergiadigital.taxsi.pasajero.InternalData.PrefsTaxsi;
import com.sinergiadigital.taxsi.pasajero.R;
import com.sinergiadigital.taxsi.pasajero.Utils.MessageUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EvaluateActivity extends BaseActivity implements EvaluatePresenter.EvaluatePasseengerCallback {
    @BindView(R.id.mSendQualifyButton)
    public Button mSendQualifyButton;
    @BindView(R.id.mCommentTextView)
    public EditText mCommentTextView;
    @BindView(R.id.NamePassengerTextView)
    public TextView NamePassengerTextView;

    @BindView(R.id.mOneStarImageView)
    public ImageView mOneStarImageView;
    @BindView(R.id.mTwoStarImageView)
    public ImageView mTwoStarImageView;
    @BindView(R.id.mThreeStarImageView)
    public ImageView mThreeStarImageView;
    @BindView(R.id.mFourStarImageView)
    public ImageView mFourStarImageView;
    @BindView(R.id.mFiveStarImageView)
    public ImageView mFiveStarImageView;

    private EvaluatePresenter mEvaluatePassengerPresenter;
    private PrefsTaxsi mPrefsTaxsi;
    private String valoracion = "5";
    private String id_viaje= "";
    private String nombre_pasajero = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.qualify_dialog);
        ButterKnife.bind(this);
        mPrefsTaxsi = new PrefsTaxsi(this);
        id_viaje = getIntent().getStringExtra("id_viaje");
        nombre_pasajero = getIntent().getStringExtra("nombre_pasajero");
        Log.d("id_viaje",id_viaje);
        NamePassengerTextView.setText("Evalua a "+nombre_pasajero);
        defaultEvaluation();
    }

    @OnClick(R.id.mOneStarLinearLayout)
    public void OnClickOneStart(){
        valoracion = "1";
        mOneStarImageView.setImageResource(R.drawable.starfilled);
        mTwoStarImageView.setImageResource(R.drawable.starunfilled);
        mThreeStarImageView.setImageResource(R.drawable.starunfilled);
        mFourStarImageView.setImageResource(R.drawable.starunfilled);
        mFiveStarImageView.setImageResource(R.drawable.starunfilled);
    }

    @OnClick(R.id.mTwoStarLinearLayout)
    public void OnClickTwoStart(){
        valoracion = "2";
        mOneStarImageView.setImageResource(R.drawable.starfilled);
        mTwoStarImageView.setImageResource(R.drawable.starfilled);
        mThreeStarImageView.setImageResource(R.drawable.starunfilled);
        mFourStarImageView.setImageResource(R.drawable.starunfilled);
        mFiveStarImageView.setImageResource(R.drawable.starunfilled);

    }

    @OnClick(R.id.mThreeStarLinearLayout)
    public void OnClickThreeStart(){
        valoracion = "3";
        mOneStarImageView.setImageResource(R.drawable.starfilled);
        mTwoStarImageView.setImageResource(R.drawable.starfilled);
        mThreeStarImageView.setImageResource(R.drawable.starfilled);
        mFourStarImageView.setImageResource(R.drawable.starunfilled);
        mFiveStarImageView.setImageResource(R.drawable.starunfilled);
    }

    @OnClick(R.id.mFourStarLinearLayout)
    public void OnClickFourStart(){
        valoracion = "4";
        mOneStarImageView.setImageResource(R.drawable.starfilled);
        mTwoStarImageView.setImageResource(R.drawable.starfilled);
        mThreeStarImageView.setImageResource(R.drawable.starfilled);
        mFourStarImageView.setImageResource(R.drawable.starfilled);
        mFiveStarImageView.setImageResource(R.drawable.starunfilled);
    }

    @OnClick(R.id.mFiveStarLinearLayout)
    public void OnClickFiveStart(){
        valoracion = "5";
        mOneStarImageView.setImageResource(R.drawable.starfilled);
        mTwoStarImageView.setImageResource(R.drawable.starfilled);
        mThreeStarImageView.setImageResource(R.drawable.starfilled);
        mFourStarImageView.setImageResource(R.drawable.starfilled);
        mFiveStarImageView.setImageResource(R.drawable.starfilled);
    }

    public void defaultEvaluation(){
        valoracion = "3";
        mOneStarImageView.setImageResource(R.drawable.starfilled);
        mTwoStarImageView.setImageResource(R.drawable.starfilled);
        mThreeStarImageView.setImageResource(R.drawable.starfilled);
        mFourStarImageView.setImageResource(R.drawable.starunfilled);
        mFiveStarImageView.setImageResource(R.drawable.starunfilled);
    }

    @OnClick(R.id.mSendQualifyButton)
    public void OnClickSendQualify(){
        mEvaluatePassengerPresenter.evaluate(mPrefsTaxsi.getData("id_pasajero"),id_viaje,valoracion,mCommentTextView.getText().toString());
    }

    @Override
    protected BasePresenter getPresenter() {
        mEvaluatePassengerPresenter = new EvaluatePresenter(this, this);
        return mEvaluatePassengerPresenter;
    }

    @Override
    public void OnSuccessEvaluate() {
        MessageUtils.toast(this, "Evaluacion enviada correctamente");
        this.finish();
    }
}

