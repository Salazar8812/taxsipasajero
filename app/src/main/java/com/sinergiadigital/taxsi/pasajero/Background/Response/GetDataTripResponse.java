package com.sinergiadigital.taxsi.pasajero.Background.Response;

import com.google.gson.annotations.SerializedName;
import com.sinergiadigital.taxsi.pasajero.Background.WSBaseResponseInterface;
import com.sinergiadigital.taxsi.pasajero.Model.Viaje;

import java.util.ArrayList;
import java.util.List;

public class GetDataTripResponse implements WSBaseResponseInterface {
    @SerializedName("viaje")
    public List<Viaje> viaje = new ArrayList<>();
    @SerializedName("resultado")
    public List<PendingEvaluateResponse.Resultado> resultado = new ArrayList<>();
}
