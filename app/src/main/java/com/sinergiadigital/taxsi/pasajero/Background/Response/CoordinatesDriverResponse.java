package com.sinergiadigital.taxsi.pasajero.Background.Response;

import com.google.gson.annotations.SerializedName;
import com.sinergiadigital.taxsi.pasajero.Background.WSBaseResponseInterface;
import com.sinergiadigital.taxsi.pasajero.Model.Resultado;
import com.sinergiadigital.taxsi.pasajero.Model.Travel;

public class CoordinatesDriverResponse implements WSBaseResponseInterface {
    @SerializedName("resultadoServiceCall")
    public Resultado resultadoServiceCall;

    @SerializedName("viaje")
    public Travel travel;
}
