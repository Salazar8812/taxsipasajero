package com.sinergiadigital.taxsi.pasajero.Model;

import com.google.gson.annotations.SerializedName;

public class LocationPlace {
    @SerializedName("lat")
    public String lat;

    @SerializedName("lng")
    public String lng;


}
