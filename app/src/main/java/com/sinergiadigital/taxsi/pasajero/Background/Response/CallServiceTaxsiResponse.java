package com.sinergiadigital.taxsi.pasajero.Background.Response;

import com.google.gson.annotations.SerializedName;
import com.sinergiadigital.taxsi.pasajero.Background.WSBaseResponseInterface;

import java.util.List;

public class CallServiceTaxsiResponse implements WSBaseResponseInterface {
    @SerializedName("resultadoServiceCall")
    public List<ResultadoServiceCall> resultadoServiceCall;
}
