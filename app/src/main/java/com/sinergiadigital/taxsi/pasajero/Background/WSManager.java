package com.sinergiadigital.taxsi.pasajero.Background;

import com.sinergiadigital.taxsi.pasajero.Background.Request.CalculatePriceRequest;
import com.sinergiadigital.taxsi.pasajero.Background.Request.CallServiceTaxsiRequest;
import com.sinergiadigital.taxsi.pasajero.Background.Request.CoordinatesDriverRequest;
import com.sinergiadigital.taxsi.pasajero.Background.Request.EvaluateRequest;
import com.sinergiadigital.taxsi.pasajero.Background.Request.LoginRequest;
import com.sinergiadigital.taxsi.pasajero.Background.Request.PendingEvaluateRequest;
import com.sinergiadigital.taxsi.pasajero.Background.Request.PredictionRequest;
import com.sinergiadigital.taxsi.pasajero.Background.Request.RegisterRequest;
import com.sinergiadigital.taxsi.pasajero.Background.Request.TakeTripRequest;
import com.sinergiadigital.taxsi.pasajero.Background.Request.UpdatePasswordRequest;

import okhttp3.ResponseBody;
import retrofit2.Call;

/**
 * Created by charlssalazar on 29/05/18.
 */

public class WSManager extends BaseWSManager {

    public static boolean DEBUG_ENABLED = false;

    public static WSManager init() {
        return new WSManager();
    }

    public class WS {
        public static final String LOGIN = "login";
        public static final String AUTOCOMPLETE = "prediction";
        public static final String GETLATLNG = "getDetailLatLng";
        public static final String CALL_SERVICE_TAXSI = "callServiceTaxsi";
        public static final String GET_COORDINATES_DRIVER = "getCoordinatesDriver";
        public static final String REGISTER_USER= "regsiter_user";
        public static final String TAKETRIP = "trip";
        public static final String INIT_TRIP = "initTrip";
        public static final String END_TRIP = "endTrip";
        public static final String EVUALATE_PASSENGER = "evaluate_passenger";
        public static final String UPDATE_DATA_PASSENGER = "actualiza_datos_pasajero";
        public static final String UPDATE_PASSWORD_PASSENGER = "actualiza_contraseña";


        public static final String FAVORITES = "favoritos";
        public static final String GET_DATA_CONSULTING = "get_data_driver";
        public static final String GET_DATA_TRIP = "get_data_trip";
        public static final String CANCEL_TRIP = "cancel_trio";
        public static final String SEND_LOCATION_DRIVER = "send_location_driver";
        public static final String HISTORY_TRAVELS = "history";
        public static final String GET_ESTATUS_DRIVER = "get_status_driver";
        public static final String GET_PENDING_TRAVEL = "get_pending_travel";
        public static final String GET_DATA_TRAVEL_EN_RUTA_DE_RECOLECCION = "get_data_en_ruta_de_recoleccion";
        public static final String GET_DATA_TRABEL_EN_VIAJE_DESTINO = "get_data_en_viaje_destino";
        public static final String GET_DATA_CANCEL_LISTENER = "get_data_travel_cancel_listener";
        public static final String GET_DATA_FROM_NOTIFICATION = "get_data_from_notificacion";
        public static final String SEND_NOTIFICATION = "send_notification";
        public static final String CHANGE_PASSWORD  = "change_password";
        public static final String CALCULAR_TARIFA  = "calcular_tarifa";
        public static final String GET_CENTRAL_PHONES = "get_central_phone";
        public static final String ALERT_NEAR_TRAVEL = "alert_near_travel";

    }

    @Override
    protected Call<ResponseBody> getWebService(String webServiceValue, WSBaseRequestInterface wsBaseRequest) {
        Call<ResponseBody> call = null;
        TaxsiApiDefinition taxiWebServicesDefinition = WebServices.taxsiApiDefinitions();
        GoogleMapServiceDefinitions googleMapServiceDefinitions = WebServices.mapsServices();
        switch (webServiceValue) {
            case WS.LOGIN:
                LoginRequest loginRequest = (LoginRequest) wsBaseRequest;
                call = taxiWebServicesDefinition.getLogin(loginRequest.mIdType,loginRequest.email,loginRequest.password);
                break;
            case WS.AUTOCOMPLETE:
                PredictionRequest predictionRequest = (PredictionRequest) wsBaseRequest;
                call = googleMapServiceDefinitions.autoComplete(predictionRequest.mInput,predictionRequest.mKey);
                break;
            case WS.GETLATLNG:
                PredictionRequest mGetPrediction = (PredictionRequest) wsBaseRequest;
                call = googleMapServiceDefinitions.getDetailPlace(mGetPrediction.mInput,mGetPrediction.mKey);
                break;

            case WS.CALL_SERVICE_TAXSI:
                CallServiceTaxsiRequest callServiceTaxsiRequest = (CallServiceTaxsiRequest) wsBaseRequest;
                call = taxiWebServicesDefinition.requestTravel(callServiceTaxsiRequest.id_pasassenger,
                        callServiceTaxsiRequest.lugar_partida,callServiceTaxsiRequest.lugar_llegada,
                        callServiceTaxsiRequest.latitud_partida,
                        callServiceTaxsiRequest.longitud_partida,
                        callServiceTaxsiRequest.latitud_llegada_sol,
                        callServiceTaxsiRequest.longitud_llegada_sol);
                break;
            case WS.CALCULAR_TARIFA:
                CalculatePriceRequest mCalcularTarifaRequest = (CalculatePriceRequest) wsBaseRequest;
                call = taxiWebServicesDefinition.calculatePrice(mCalcularTarifaRequest.distancia,mCalcularTarifaRequest.tiempo);
                break;

            case WS.GET_COORDINATES_DRIVER:
                CoordinatesDriverRequest coordinatesDriverRequest = (CoordinatesDriverRequest) wsBaseRequest;
                call = taxiWebServicesDefinition.getCoordinatesTravel(coordinatesDriverRequest.id_viaje);
                break;
            case WS.GET_PENDING_TRAVEL:
                PendingEvaluateRequest pendingEvaluateRequest = (PendingEvaluateRequest) wsBaseRequest;
                call = taxiWebServicesDefinition.pendingEvaluatePassenger(pendingEvaluateRequest.id_pasajero);
                break;

            case WS.REGISTER_USER:
                RegisterRequest registerRequest = (RegisterRequest) wsBaseRequest;
                call = taxiWebServicesDefinition.register(registerRequest.mType,
                        registerRequest.mName,
                        registerRequest.mLastName,
                        registerRequest.mGenre,
                        registerRequest.mAge,
                        registerRequest.mEmail,
                        registerRequest.mCellPhone,
                        registerRequest.mPassword,
                        registerRequest.mPhoto);
                break;
            case WS.EVUALATE_PASSENGER:
                EvaluateRequest evaluateRequest = (EvaluateRequest) wsBaseRequest;
                call = taxiWebServicesDefinition.evaluateDriver(evaluateRequest.id_pasajero,evaluateRequest.id_viaje, evaluateRequest.valoracion,evaluateRequest.comentario);
                break;
            case WS.HISTORY_TRAVELS:
                TakeTripRequest takeTripRequest = (TakeTripRequest) wsBaseRequest;
                call = taxiWebServicesDefinition.getHistoryTravelPassenger(takeTripRequest.id_conductor);
                break;

            case WS.FAVORITES:
                TakeTripRequest favsRequest = (TakeTripRequest) wsBaseRequest;
                call = taxiWebServicesDefinition.getPlacePassenger(favsRequest.id_conductor);
                break;

            case WS.UPDATE_DATA_PASSENGER:
                RegisterRequest updateDatarequest = (RegisterRequest) wsBaseRequest;
                call = taxiWebServicesDefinition.updatePassenger(updateDatarequest.id_pasajero,
                        updateDatarequest.mName,
                        updateDatarequest.mLastName,
                        updateDatarequest.mCellPhone,
                        updateDatarequest.mEmail,
                        updateDatarequest.mGenre,
                        updateDatarequest.mAge,
                        updateDatarequest.mPhoto);
                break;
            case WS.UPDATE_PASSWORD_PASSENGER:
                UpdatePasswordRequest updatePasswordRequest = (UpdatePasswordRequest) wsBaseRequest;
                call = taxiWebServicesDefinition.updatePasswordPassenger(updatePasswordRequest.id_pasajero,
                        updatePasswordRequest.viejo_pass,
                        updatePasswordRequest.nuevo_pass);
                break;

        }
        return call;
    }

    @Override
    protected Call<ResponseBody> getQueryWebService(String webServiceValue, String requestValue) {
        return null;
    }

    @Override
    protected String getJsonDebug(String requestUrl) {
        return null;
    }

    @Override
    protected boolean getErrorDebugEnabled() {
        return false;
    }

    @Override
    protected boolean getDebugEnabled() {
        return false;
    }
}

