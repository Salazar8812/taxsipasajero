package com.sinergiadigital.taxsi.pasajero.Background.Response;

import com.google.gson.annotations.SerializedName;
import com.sinergiadigital.taxsi.pasajero.Background.WSBaseResponseInterface;
import com.sinergiadigital.taxsi.pasajero.Model.Passenger;

import java.util.List;

public class RegisterResponse implements WSBaseResponseInterface {
    @SerializedName("pasajero")
    public List<Passenger> passenger;
}
