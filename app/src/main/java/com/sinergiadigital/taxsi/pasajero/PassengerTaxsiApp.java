package com.sinergiadigital.taxsi.pasajero;

import android.app.Application;
import android.support.multidex.MultiDex;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.sinergiadigital.taxsi.pasajero.GPSService.BusManager;

public class PassengerTaxsiApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        BusManager.init();
        MultiDex.install(this);
    }
}
