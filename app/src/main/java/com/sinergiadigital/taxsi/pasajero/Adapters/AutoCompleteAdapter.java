package com.sinergiadigital.taxsi.pasajero.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.sinergiadigital.taxsi.pasajero.Background.Response.PredictionsResponse;
import com.sinergiadigital.taxsi.pasajero.R;
import java.util.ArrayList;
import java.util.List;

public class AutoCompleteAdapter extends RecyclerView.Adapter<AutoCompleteHolder>{
    private List<PredictionsResponse.PredictionsEntity> mListPrediction = new ArrayList<>();
    private List<AutoCompleteHolder> mViewHolders = new ArrayList<>();

    private ItemCallback mItemCallback;
    private String mTypeSearch;


    public AutoCompleteAdapter(ItemCallback mItemCallback, String mTypeSearch) {
        this.mItemCallback = mItemCallback;
        this.mTypeSearch = mTypeSearch;
    }

    @Override
    public AutoCompleteHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return insertViewHolder(new AutoCompleteHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_autocomplete,
                        parent, false)));
    }

    private AutoCompleteHolder insertViewHolder(@NonNull AutoCompleteHolder vh) {
        if (!mViewHolders.contains(vh)) {
            mViewHolders.add(vh);
        }
        return vh;
    }


    public void update(List<PredictionsResponse.PredictionsEntity> mListPrediction){
        this.mListPrediction = mListPrediction;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(AutoCompleteHolder holder, int position) {
        holder.render(mListPrediction.get(position), false, mItemCallback,mTypeSearch);
    }

    @Override
    public int getItemCount() {
        return mListPrediction.size();
    }


    public interface ItemCallback{
        void OnGetItemClick(AutoCompleteHolder viewHolder, String mType);
    }
}
