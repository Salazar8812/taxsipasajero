package com.sinergiadigital.taxsi.pasajero.Adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sinergiadigital.taxsi.pasajero.Model.Viaje;
import com.sinergiadigital.taxsi.pasajero.R;

import java.util.ArrayList;
import java.util.List;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.HistoryHolder> {
    private Activity activity;
    private List<Viaje> listHistory = new ArrayList<>();

    public HistoryAdapter(Activity activity, List<Viaje> listHistory){
        this.activity = activity;
        this.listHistory = listHistory;
    }

    @Override
    public HistoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_history, parent, false);
        HistoryHolder vHolder = new HistoryHolder(layoutView);
        return vHolder;
    }

    @Override
    public void onBindViewHolder(HistoryHolder holder, int position) {
        holder.mDateTripTextView.setText(listHistory.get(position).fechaSolicitud);
        holder.mCancelTripTextView.setText(listHistory.get(position).estatus);
        holder.mOriginTripTextView.setText(listHistory.get(position).lugarPartida);
        holder.mArriveTextView.setText(listHistory.get(position).lugarLlegada);
        holder.mNameDriverTextView.setText(listHistory.get(position).nombreConductor);

    }

    @Override
    public int getItemCount() {
        return listHistory.size();
    }
    class HistoryHolder extends RecyclerView.ViewHolder{
        private TextView mDateTripTextView;
        private TextView mCancelTripTextView;
        private TextView mOriginTripTextView;
        private TextView mArriveTextView;
        private TextView mNameDriverTextView;

        public HistoryHolder(View itemView) {
            super(itemView);
            mDateTripTextView = itemView.findViewById(R.id.mDateTripTextView);
            mCancelTripTextView = itemView.findViewById(R.id.mCancelTripTextView);
            mOriginTripTextView = itemView.findViewById(R.id.mOriginTripTextView);
            mArriveTextView = itemView.findViewById(R.id.mArriveTextView);
            mNameDriverTextView = itemView.findViewById(R.id.mNameDriverTextView);
        }
    }
}
