package com.sinergiadigital.taxsi.pasajero.Activities;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sinergiadigital.taxsi.pasajero.Adapters.FavsAdapter;
import com.sinergiadigital.taxsi.pasajero.Background.Response.Lugares;
import com.sinergiadigital.taxsi.pasajero.BaseClasses.BaseActivity;
import com.sinergiadigital.taxsi.pasajero.BaseClasses.BasePresenter;
import com.sinergiadigital.taxsi.pasajero.Implementation.FavsPresenter;
import com.sinergiadigital.taxsi.pasajero.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FavsActivity extends BaseActivity implements FavsPresenter.FavsCallback {
    @BindView(R.id.mLeftMenuLinearLayout)
    public LinearLayout mLeftMenuLinearLayout;
    @BindView(R.id.mTitleToolBarTextField)
    public TextView mTitleToolBarTextField;
    @BindView(R.id.mBackImageView)
    public ImageView mBackImageView;
    @BindView(R.id.mRichIconImageView)
    public ImageView mRichIconImageView;
    @BindView(R.id.mRightMenuLinearLayout)
    public LinearLayout mRightMenuLinearLayout;

    @BindView(R.id.mHistoryListRecyclerView)
    public RecyclerView mHistoryListRecyclerView;
    private FavsAdapter mFavsAdapter;
    private FavsPresenter mFavsPresenter;
    private List<Lugares> mListFavs = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.history_activity);
        ButterKnife.bind(this);
        configureRecyclerView();
        setToolBar();
        mFavsPresenter.getFavs();
    }

    private void configureRecyclerView(){
        mHistoryListRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mFavsAdapter = new FavsAdapter();
        mHistoryListRecyclerView.setAdapter(mFavsAdapter);
        mFavsAdapter.update(mListFavs);
    }

    @Override
    protected BasePresenter getPresenter() {
        mFavsPresenter = new FavsPresenter(this,this);
        return mFavsPresenter;
    }

    public void setToolBar(){
        setTitle("Favoritos");
        mBackImageView.setImageResource(R.drawable.ic_back_icon);
        mRichIconImageView.setVisibility(View.INVISIBLE);
    }

    public void setTitle(String mTitle){
        mTitleToolBarTextField.setText(mTitle);
    }

    @OnClick(R.id.mBackImageView)
    public void OnClickBack(){
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void OnSuccessGetFavs(List<Lugares> mListLugares) {
        mFavsAdapter.update(mListLugares);
    }
}
