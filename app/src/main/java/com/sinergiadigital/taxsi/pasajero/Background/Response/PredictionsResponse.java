package com.sinergiadigital.taxsi.pasajero.Background.Response;

import com.google.gson.annotations.SerializedName;
import com.sinergiadigital.taxsi.pasajero.Background.WSBaseResponseInterface;
import com.sinergiadigital.taxsi.pasajero.Model.StructuredFormatting;

import java.util.ArrayList;
import java.util.List;

public class PredictionsResponse implements WSBaseResponseInterface {
    @SerializedName("status")
    public String status;
    @SerializedName("predictions")
    public List<PredictionsEntity> predictions = new ArrayList<>();

    public static class PredictionsEntity {
        @SerializedName("description")
        public String description;
        @SerializedName("id")
        public String id;
        @SerializedName("place_id")
        public String placeId;
        @SerializedName("reference")
        public String reference;
        @SerializedName("matched_substrings")
        public List<MatchedSubstringsEntity> matchedSubstrings;
        @SerializedName("terms")
        public List<TermsEntity> terms;
        @SerializedName("types")
        public List<String> types;

        @SerializedName("structured_formatting")
        public StructuredFormatting structuredFormatting;

        public static class MatchedSubstringsEntity {
            @SerializedName("length")
            public int length;
            @SerializedName("offset")
            public int offset;
        }

        public static class TermsEntity {
            @SerializedName("offset")
            public int offset;
            @SerializedName("value")
            public String value;
        }
    }
}
