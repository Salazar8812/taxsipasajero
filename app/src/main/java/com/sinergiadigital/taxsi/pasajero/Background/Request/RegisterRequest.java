package com.sinergiadigital.taxsi.pasajero.Background.Request;

import com.sinergiadigital.taxsi.pasajero.Background.WSBaseRequestInterface;

public class RegisterRequest implements WSBaseRequestInterface {
    public String id_pasajero;
    public String mType;
    public String mName;
    public String mLastName;
    public String mGenre;
    public String mAge;
    public String mEmail;
    public String mCellPhone;
    public String mPassword;
    public String mPhoto;

    public RegisterRequest(String mType, String mName, String mLastName, String mGenre, String mAge, String mEmail, String mCellPhone, String mPassword, String mPhoto) {
        this.mType = mType;
        this.mName = mName;
        this.mLastName = mLastName;
        this.mGenre = mGenre;
        this.mAge = mAge;
        this.mEmail = mEmail;
        this.mCellPhone = mCellPhone;
        this.mPassword = mPassword;
        this.mPhoto = mPhoto;
    }

    public RegisterRequest(String id_pasajero, String mName, String mLastName, String mGenre, String mAge, String mEmail, String mCellPhone, String mPhoto) {
        this.id_pasajero = id_pasajero;
        this.mName = mName;
        this.mLastName = mLastName;
        this.mGenre = mGenre;
        this.mAge = mAge;
        this.mEmail = mEmail;
        this.mCellPhone = mCellPhone;
        this.mPhoto = mPhoto;
    }
}
