package com.sinergiadigital.taxsi.pasajero.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.sinergiadigital.taxsi.pasajero.Adapters.GenreAdapter;
import com.sinergiadigital.taxsi.pasajero.BaseClasses.BaseActivity;
import com.sinergiadigital.taxsi.pasajero.BaseClasses.BasePresenter;
import com.sinergiadigital.taxsi.pasajero.Implementation.RegisterPresenter;
import com.sinergiadigital.taxsi.pasajero.R;
import com.sinergiadigital.taxsi.pasajero.Utils.EditTextValidator;
import com.sinergiadigital.taxsi.pasajero.Utils.FormValidator;
import com.sinergiadigital.taxsi.pasajero.Utils.Regex;
import com.sinergiadigital.taxsi.pasajero.Utils.UpperCaseCharacter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterActivity extends BaseActivity implements RegisterPresenter.RegisterUserCallback {
    @BindView(R.id.mNameEditText)
    public EditText mNameEditTex;

    @BindView(R.id.mLastNameEditText)
    public EditText mLastNameEditText;

    @BindView(R.id.mGenreTextView)
    public TextView mGenreTextView;

    @BindView(R.id.mAgeEditTex)
    public EditText mAgeEditTex;

    @BindView(R.id.mEmaiEditText)
    public EditText mEmaiEditText;

    @BindView(R.id.mCellPhoneEditText)
    public EditText mCellPhoneEditText;

    @BindView(R.id.mPasswordEditText)
    public EditText mPasswordEditText;

    private RegisterPresenter registerPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_activity);
        ButterKnife.bind(this);
        mFormValidator = new FormValidator(this);

        mFormValidator.addValidators(
                new EditTextValidator(mNameEditTex, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mLastNameEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mAgeEditTex, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mEmaiEditText, Regex.EMAIL, R.string.dialog_error_empty),
                new EditTextValidator(mCellPhoneEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mPasswordEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty)
        );

    }

    private void showDialog(){
        startActivityForResult(new Intent(this, GenreDialogActivity.class), 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            mGenreTextView.setText(data.getStringExtra("RESULT_STRING"));
        }catch (NullPointerException e){
            e.printStackTrace();
        }
    }

    @Override
    protected BasePresenter getPresenter() {
        registerPresenter = new RegisterPresenter(this,this);
        return registerPresenter;
    }

    public void register(){
        registerPresenter.registerUser("1",mNameEditTex.getText().toString(),
                mLastNameEditText.getText().toString(),
                mGenreTextView.getText().toString(),
                mAgeEditTex.getText().toString(),
                mEmaiEditText.getText().toString(),
                mCellPhoneEditText.getText().toString(),
                mPasswordEditText.getText().toString(),
                "");
    }


    @OnClick({R.id.mRegisterButton,R.id.mGenreTextView})
    public void OnClickRegister(View view){

        switch (view.getId()){
            case R.id.mGenreTextView:
                showDialog();
                break;
            case R.id.mRegisterButton:
                if(mFormValidator.isValid() && mGenreTextView.getText() != "") {
                    register();
                }else{
                    Toast.makeText(this, "Es necesario ingresar todos los campos", Toast.LENGTH_LONG).show();
                }
                break;
        }

    }


    @Override
    public void OnSuccessRegister() {
        startActivity(new Intent(this,MainMapActivity.class));
    }

}
