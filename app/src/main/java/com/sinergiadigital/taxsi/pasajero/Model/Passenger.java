package com.sinergiadigital.taxsi.pasajero.Model;

import com.google.gson.annotations.SerializedName;

public class Passenger {
    @SerializedName("id")
    public String id;

    @SerializedName("nombre")
    public String nombre;

    @SerializedName("apellidos")
    public String apellidos;

    @SerializedName("edad")
    public String edad;

    @SerializedName("email")
    public String email;

    @SerializedName("sexo")
    public String sexo;

    @SerializedName("telefono")
    public String telefono;
}
