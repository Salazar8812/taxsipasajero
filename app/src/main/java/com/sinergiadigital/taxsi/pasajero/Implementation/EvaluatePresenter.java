package com.sinergiadigital.taxsi.pasajero.Implementation;

import android.support.v7.app.AppCompatActivity;

import com.sinergiadigital.taxsi.pasajero.Background.BaseWSManager;
import com.sinergiadigital.taxsi.pasajero.Background.Request.EvaluateRequest;
import com.sinergiadigital.taxsi.pasajero.Background.Response.GetDataTripResponse;
import com.sinergiadigital.taxsi.pasajero.Background.WSBaseResponseInterface;
import com.sinergiadigital.taxsi.pasajero.Background.WSCallback;
import com.sinergiadigital.taxsi.pasajero.Background.WSManager;
import com.sinergiadigital.taxsi.pasajero.Utils.MessageUtils;

public class EvaluatePresenter extends TaxsiPresenter implements WSCallback {

    private EvaluatePasseengerCallback mEvaluatePasseengerCallback;

    public interface EvaluatePasseengerCallback{
        void OnSuccessEvaluate();
    }

    public EvaluatePresenter(AppCompatActivity appCompatActivity, EvaluatePasseengerCallback mEvaluatePasseengerCallback) {
        super(appCompatActivity);
        this.mEvaluatePasseengerCallback = mEvaluatePasseengerCallback;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public BaseWSManager initWSManager() {
        return mWSManager = WSManager.init().settings(mContext, this);
    }

    public void evaluate(String id_conductor,String id_viaje, String valoracion,String comentario){
        EvaluateRequest takeTripRequest = new EvaluateRequest(id_conductor,id_viaje,valoracion,comentario);
        mWSManager.requestWs(GetDataTripResponse.class, WSManager.WS.EVUALATE_PASSENGER, takeTripRequest);
    }

    private void OnSuccessEvaluate(GetDataTripResponse mGetDataTripResponse){
        if(mGetDataTripResponse != null) {
            if (mGetDataTripResponse.resultado.get(0).resultado.equals("1")) {
                MessageUtils.stopProgress();
                mEvaluatePasseengerCallback.OnSuccessEvaluate();
            } else {
                MessageUtils.stopProgress();
                MessageUtils.toast(mAppCompatActivity, "Ocurrio un error al enviar la evaluacion, intente nuevamente");
            }
        }else{
            MessageUtils.stopProgress();
            MessageUtils.toast(mAppCompatActivity, "Ocurrio un error al enviar la evaluacion, intente nuevamente o mas tarde");
        }
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        if(requestUrl.equals(WSManager.WS.EVUALATE_PASSENGER)){
            OnSuccessEvaluate((GetDataTripResponse) baseResponse);
        }
    }
}
