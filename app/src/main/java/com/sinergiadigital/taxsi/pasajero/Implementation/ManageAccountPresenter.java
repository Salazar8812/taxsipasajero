package com.sinergiadigital.taxsi.pasajero.Implementation;

import android.support.v7.app.AppCompatActivity;

import com.sinergiadigital.taxsi.pasajero.Background.BaseWSManager;
import com.sinergiadigital.taxsi.pasajero.Background.Request.RegisterRequest;
import com.sinergiadigital.taxsi.pasajero.Background.Request.UpdatePasswordRequest;
import com.sinergiadigital.taxsi.pasajero.Background.Response.PassengerResponse;
import com.sinergiadigital.taxsi.pasajero.Background.Response.UpdatePasswordResponse;
import com.sinergiadigital.taxsi.pasajero.Background.WSBaseResponseInterface;
import com.sinergiadigital.taxsi.pasajero.Background.WSManager;
import com.sinergiadigital.taxsi.pasajero.InternalData.PrefsTaxsi;
import com.sinergiadigital.taxsi.pasajero.Model.Passenger;

public class ManageAccountPresenter extends TaxsiPresenter {

    private PrefsTaxsi mPrefsTaxsi;

    public ManageAccountPresenter(AppCompatActivity appCompatActivity) {
        super(appCompatActivity);
        mPrefsTaxsi = new PrefsTaxsi(mAppCompatActivity);
    }

    @Override
    public BaseWSManager initWSManager() {
        return mWSManager = WSManager.init().settings(mContext, this);
    }

    public void updateDataPassenger(String id_pasajero,String mName, String mLastName, String mAge, String mGenre, String mPhone, String mEmail,String mPhoto){
        RegisterRequest registerRequest = new RegisterRequest(id_pasajero, mName,mLastName,mGenre,mAge,mEmail,mPhone,mPhoto);
        mWSManager.requestWs(PassengerResponse.class, WSManager.WS.UPDATE_DATA_PASSENGER, registerRequest);
    }

    public void updatePassword(String id_pasajero, String viejo_pass, String nuevo_pass){
        UpdatePasswordRequest updatePasswordRequest = new UpdatePasswordRequest(id_pasajero,viejo_pass,nuevo_pass);
        mWSManager.requestWs(UpdatePasswordResponse.class, WSManager.WS.LOGIN, updatePasswordRequest);
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        switch (requestUrl){
            case WSManager.WS.UPDATE_DATA_PASSENGER:
                OnSuccessUpdate((PassengerResponse) baseResponse);
                break;

            case WSManager.WS.UPDATE_PASSWORD_PASSENGER:
                OnSuccessUpdatePassword((UpdatePasswordResponse) baseResponse);
                break;
                default:break;
        }
    }

    public void OnSuccessUpdate(PassengerResponse mPassengerResponse){
        if(mPassengerResponse != null){
            if(mPassengerResponse.mPassengerData != null){
                saveDataPassenger(mPassengerResponse.mPassengerData.get(0));
            }
        }
    }

    public void OnSuccessUpdatePassword(UpdatePasswordResponse updatePasswordResponse){
        if(updatePasswordResponse.mResultado != null){
            if(updatePasswordResponse.mResultado.get(0).resultado.equals("0")){

            }
        }
    }

    private void saveDataPassenger(Passenger loginResponse){
        mPrefsTaxsi.saveData("id_pasajero",loginResponse.id);
        mPrefsTaxsi.saveData("nombre",loginResponse.nombre);
        mPrefsTaxsi.saveData("apellidos",loginResponse.apellidos);
        mPrefsTaxsi.saveData("email",loginResponse.email);
        mPrefsTaxsi.saveData("edad",loginResponse.edad);
        mPrefsTaxsi.saveData("telefono",loginResponse.telefono);
        mPrefsTaxsi.saveData("sexo",loginResponse.sexo);
    }
}
