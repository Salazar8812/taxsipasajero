package com.sinergiadigital.taxsi.pasajero.Background.Request;

import com.sinergiadigital.taxsi.pasajero.Background.WSBaseRequestInterface;

public class LoginRequest implements WSBaseRequestInterface {
    public String mIdType;
    public String email;
    public String password;

    public LoginRequest(String mIdType, String email, String password) {
        this.mIdType = mIdType;
        this.email = email;
        this.password = password;
    }
}
