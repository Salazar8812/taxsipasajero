package com.sinergiadigital.taxsi.pasajero.Model;

import com.google.gson.annotations.SerializedName;

public class StructuredFormatting {
    @SerializedName("main_text")
    public String main_text;

    @SerializedName("secondary_text")
    public String secondary_text;
}
