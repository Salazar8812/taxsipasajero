package com.sinergiadigital.taxsi.pasajero.Implementation;

import android.support.v7.app.AppCompatActivity;

import com.sinergiadigital.taxsi.pasajero.Background.BaseWSManager;
import com.sinergiadigital.taxsi.pasajero.Background.Request.TakeTripRequest;
import com.sinergiadigital.taxsi.pasajero.Background.Response.GetDataTripResponse;
import com.sinergiadigital.taxsi.pasajero.Background.WSBaseResponseInterface;
import com.sinergiadigital.taxsi.pasajero.Background.WSCallback;
import com.sinergiadigital.taxsi.pasajero.Background.WSManager;
import com.sinergiadigital.taxsi.pasajero.Model.Viaje;
import com.sinergiadigital.taxsi.pasajero.Utils.MessageUtils;

import java.util.List;

public class HistoryPresenter extends TaxsiPresenter implements WSCallback {

    public interface HistoryCallback{
        void OnSuccessHistory(List<Viaje> listViaje);
    }

    private HistoryCallback mHistoryCallback;
    public HistoryPresenter(AppCompatActivity appCompatActivity, HistoryCallback historyCallback)  {
        super(appCompatActivity);
        this.mHistoryCallback = historyCallback;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public BaseWSManager initWSManager() {
        return mWSManager = WSManager.init().settings(mContext, this);
    }

    public void getHistorial(String id_pasajero,String id_viaje){
        MessageUtils.progress(mAppCompatActivity, "Cargando...");
        TakeTripRequest takeTripRequest = new TakeTripRequest(id_pasajero,id_viaje);
        mWSManager.requestWs(GetDataTripResponse.class, WSManager.WS.HISTORY_TRAVELS, takeTripRequest);
    }

    public void OnSuccessHistory(GetDataTripResponse getDataTripResponse){
        MessageUtils.stopProgress();
        if(getDataTripResponse.resultado != null) {
            if(getDataTripResponse.resultado.get(0).resultado.equals("1")) {
                mHistoryCallback.OnSuccessHistory(getDataTripResponse.viaje);
            }
        }
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        if(requestUrl.equals(WSManager.WS.HISTORY_TRAVELS)){
            OnSuccessHistory((GetDataTripResponse) baseResponse);
        }
    }
}
