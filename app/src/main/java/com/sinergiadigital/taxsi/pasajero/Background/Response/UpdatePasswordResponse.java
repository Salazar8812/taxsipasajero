package com.sinergiadigital.taxsi.pasajero.Background.Response;

import com.google.gson.annotations.SerializedName;
import com.sinergiadigital.taxsi.pasajero.Background.WSBaseResponseInterface;

import java.util.ArrayList;
import java.util.List;

public class UpdatePasswordResponse implements WSBaseResponseInterface {
    @SerializedName("resultado")
    public List<PendingEvaluateResponse.Resultado> mResultado = new ArrayList<>();
}
