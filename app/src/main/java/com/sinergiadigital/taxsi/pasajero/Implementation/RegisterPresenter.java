package com.sinergiadigital.taxsi.pasajero.Implementation;

import android.support.v7.app.AppCompatActivity;

import com.sinergiadigital.taxsi.pasajero.Background.BaseWSManager;
import com.sinergiadigital.taxsi.pasajero.Background.Request.RegisterRequest;
import com.sinergiadigital.taxsi.pasajero.Background.Response.RegisterResponse;
import com.sinergiadigital.taxsi.pasajero.Background.WSBaseResponseInterface;
import com.sinergiadigital.taxsi.pasajero.Background.WSManager;
import com.sinergiadigital.taxsi.pasajero.InternalData.PrefsTaxsi;
import com.sinergiadigital.taxsi.pasajero.Utils.MessageUtils;

public class RegisterPresenter extends TaxsiPresenter {
    private RegisterUserCallback mRegisterUserCallback;
    private PrefsTaxsi mPrefsTaxsi;

    public interface RegisterUserCallback{
        void OnSuccessRegister();
    }

    public RegisterPresenter(AppCompatActivity appCompatActivity, RegisterUserCallback registerUserCallback) {
        super(appCompatActivity);
        mRegisterUserCallback = registerUserCallback;
        mPrefsTaxsi = new PrefsTaxsi(mAppCompatActivity);
    }

    @Override
    public BaseWSManager initWSManager() {
        return mWSManager = WSManager.init().settings(mContext, this);
    }

    public void registerUser(String type,String mName, String mLastName, String mGenre, String mAge,String mEmail, String mCellPgone, String  mPassword, String photo){
        RegisterRequest registerRequest = new RegisterRequest(type,mName,mLastName, mGenre,mAge,mEmail,mCellPgone,mPassword,photo);
        mWSManager.requestWs(RegisterResponse.class, WSManager.WS.REGISTER_USER, registerRequest);
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        if(requestUrl.equals(WSManager.WS.REGISTER_USER)){
            OnSuccessRegister((RegisterResponse) baseResponse);
        }
    }

    public void OnSuccessRegister(RegisterResponse registerResponse){
        MessageUtils.stopProgress();
        if(registerResponse != null){
            if(!registerResponse.passenger.get(0).id.equals("0")) {
                mPrefsTaxsi.saveData("id", registerResponse.passenger.get(0).id);
                mPrefsTaxsi.saveData("name", registerResponse.passenger.get(0).nombre);
                mPrefsTaxsi.saveData("lastName", registerResponse.passenger.get(0).apellidos);
                mPrefsTaxsi.saveData("edad", registerResponse.passenger.get(0).edad);
                mPrefsTaxsi.saveData("email", registerResponse.passenger.get(0).email);
                mPrefsTaxsi.saveData("sexo", registerResponse.passenger.get(0).sexo);
                mPrefsTaxsi.saveData("telefono", registerResponse.passenger.get(0).telefono);
                mRegisterUserCallback.OnSuccessRegister();
            }else{
                MessageUtils.toast(mAppCompatActivity, "Ocurrio un error al registrar el usuario intente más tarde");
            }
        }else {
            MessageUtils.toast(mAppCompatActivity, "Ocurrio un error al registrar el usuario intente más tarde");
        }
    }
}