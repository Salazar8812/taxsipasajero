package com.sinergiadigital.taxsi.pasajero.Background;

import com.sinergiadigital.taxsi.pasajero.Background.Response.PredictionsResponse;
import com.sinergiadigital.taxsi.pasajero.Model.ResultMaps;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface TaxsiApiDefinition {
    @GET("/servicios/autenticaPasajero.php")
    Call<ResponseBody> getLogin(@Query("tipo") String tipo, @Query("email") String email, @Query("password") String password);

    @GET("/servicios/autenticaPasajero.php")
    Call<ResponseBody> register(@Query("tipo") String tipo,
                                @Query("nombre") String nombre,
                                @Query("apellidos") String apellidos,
                                @Query("sexo") String sexo,
                                @Query("edad") String edad,
                                @Query("email") String email,
                                @Query("telefono") String telefono,
                                @Query("password") String password,
                                @Query("foto") String foto);

    @GET("/servicios/solicitaViaje.php")
    Call<ResponseBody> requestTravel(@Query("C") String id_pasajero,
                                     @Query("lugar_partida") String lugar_partida,
                                     @Query("lugar_llegada") String lugar_llegada,
                                     @Query("latitud_partida") String latitud_partida,
                                     @Query("longitud_partida") String longitud_partida,
                                     @Query("latitud_llegada_sol") String latitud_llegada_sol,
                                     @Query("longitud_llegada_sol") String longitud_llegada_sol);

    @GET("/servicios/solicitaViaje2.php")
    Call<ResponseBody> requestTravel2();

    @GET("/servicios/getEstatusPasajero.php")
    Call<ResponseBody> getStatusPassenger();

    @GET("/servicios/evaluaConductor.php")
    Call<ResponseBody> evaluateDriver(@Query("id_pasajero")String id_pasajero,
                                      @Query("id_viaje")String id_viajes,
                                      @Query("valoracion")String valoracion,
                                      @Query("Comentario")String comentario);

    @GET("/servicios/pasajeroTieneCalifPendiente.php")
    Call<ResponseBody> pendingEvaluatePassenger(@Query("id_pasajero")String id_pasajero);

    @GET("/servicios/updatePasajero.php")
    Call<ResponseBody> updatePassenger(@Query("id_pasajero")String id_pasajero,
                                       @Query("nombre")String nombre,
                                       @Query("apellidos")String apellidos,
                                       @Query("telefono")String telefono,
                                       @Query("email")String email,
                                       @Query("sexo")String sexo,
                                       @Query("edad")String edad,
                                       @Query("foto")String foto);

    @GET("/servicios/updatePasswordPasajero.php")
    Call<ResponseBody> updatePasswordPassenger(@Query("id_pasajero") String id_pasajero,
                                               @Query("viejopass") String viejoPass,
                                               @Query("nuevopass") String nuevoPass);

    @GET("/servicios/getDatosViajePasajero.php")
    Call<ResponseBody> getDataTravelPassenger();

    @GET("/servicios/cancelaViaje.php")
    Call<ResponseBody> cancelTravel();

    @GET("/servicios/getCoordenadasViaje.php")
    Call<ResponseBody> getCoordinatesTravel(@Query("id_viaje")String id_viaje);

    @GET("/servicios/getHistoricoViajesPasajero.php")
    Call<ResponseBody> getHistoryTravelPassenger(@Query("id_pasajero")String id_conductor);

    @GET("/servicios/calcularTarifa.php")
    Call<ResponseBody> calculatePrice(@Query("distancia")String distancia, @Query("tiempo") String tiempo);

    @GET("/servicios/agregaLugar.php")
    Call<ResponseBody> addPlace();

    @GET("/servicios/eliminaLugar.php")
    Call<ResponseBody> deletePlace();

    @GET("/servicios/getLugaresPasajero.php")
    Call<ResponseBody> getPlacePassenger(@Query("id_pasajero") String id_pasajero);

    @GET("/servicios/cancelaViajesPendientesPasajero.php")
    Call<ResponseBody> cancelPendingTravelPassenger();

    @GET("/servicios/getTelefonoCentral.php")
    Call<ResponseBody> getCentralPhones();

}
