package com.sinergiadigital.taxsi.pasajero.Activities;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sinergiadigital.taxsi.pasajero.BaseClasses.BaseActivity;
import com.sinergiadigital.taxsi.pasajero.BaseClasses.BasePresenter;
import com.sinergiadigital.taxsi.pasajero.Implementation.ManageAccountPresenter;
import com.sinergiadigital.taxsi.pasajero.InternalData.PrefsTaxsi;
import com.sinergiadigital.taxsi.pasajero.R;
import com.sinergiadigital.taxsi.pasajero.Utils.EditTextValidator;
import com.sinergiadigital.taxsi.pasajero.Utils.FormValidator;
import com.sinergiadigital.taxsi.pasajero.Utils.Regex;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ManageAccountActivity extends BaseActivity {
    @BindView(R.id.mLeftMenuLinearLayout)
    public LinearLayout mLeftMenuLinearLayout;
    @BindView(R.id.mTitleToolBarTextField)
    public TextView mTitleToolBarTextField;
    @BindView(R.id.mBackImageView)
    public ImageView mBackImageView;
    @BindView(R.id.mRichIconImageView)
    public ImageView mRichIconImageView;
    @BindView(R.id.mRightMenuLinearLayout)
    public LinearLayout mRightMenuLinearLayout;

    @BindView(R.id.mNameEditText)
    public EditText mNameEditText;

    @BindView(R.id.mLastNameEditText)
    public EditText mLastNameEditText;

    @BindView(R.id.mGenreEditText)
    public EditText mGenreEditText;

    @BindView(R.id.mAgeEditTex)
    public EditText mAgeEditTex;

    @BindView(R.id.mEmaiEditText)
    public EditText mEmaiEditText;

    @BindView(R.id.mPhoneEditeText)
    public EditText mPhoneEditeText;

    @BindView(R.id.mPasswordOldEditText)
    public EditText mPasswordOldEditText;

    @BindView(R.id.mPasswordActiveEditText)
    public EditText mPasswordActiveEditText;

    @BindView(R.id.mPasswordRepeatEditText)
    public EditText mPasswordRepeatEditText;

    @BindView(R.id.mProfileImageView)
    ImageView mProfileImageView;

    @BindView(R.id.mNameDriverTextView)
    TextView mNameDriverTextView;

    @BindView(R.id.mPhoneTextView)
    TextView mPhoneTextView;

    @BindView(R.id.mEmailDriverTextView)
    TextView mEmailDriverTextView;

    private PrefsTaxsi mPrefsTaxsi;

    private ManageAccountPresenter mManageAccountPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.manage_account_activity);
        ButterKnife.bind(this);
        setToolBar();
        mPrefsTaxsi = new PrefsTaxsi(this);
        setDataPassenger();
    }

    public void validateFieldsData(){
        mFormValidator = new FormValidator(this);
        mFormValidator.addValidators(
                new EditTextValidator(mNameEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mLastNameEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mGenreEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mAgeEditTex, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mEmaiEditText, Regex.EMAIL, R.string.dialog_error_empty),
                new EditTextValidator(mPhoneEditeText, Regex.NOT_EMPTY, R.string.dialog_error_empty)
        );
    }


    public void validateFieldsPassword(){
        mFormValidator = new FormValidator(this);
        mFormValidator.addValidators(
                new EditTextValidator(mPasswordOldEditText, Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mPasswordActiveEditText,Regex.NOT_EMPTY, R.string.dialog_error_empty),
                new EditTextValidator(mPasswordRepeatEditText,Regex.NOT_EMPTY,R.string.dialog_error_empty)
        );

    }

    @Override
    protected BasePresenter getPresenter() {
        mManageAccountPresenter = new ManageAccountPresenter(this);
        return mManageAccountPresenter;
    }

    @OnClick({R.id.mUpdateDataAccount, R.id.mChangePasswordButton})
    public void OnClickManageAccount(View view){
        switch (view.getId()){
            case R.id.mUpdateDataAccount:
                validateFieldsData();
                UpdateDataPassenger();
                break;
            case R.id.mChangePasswordButton:
                validateFieldsPassword();
                UpdatePasswordPassenger();
                break;
                default:break;
        }
    }

    public void UpdateDataPassenger(){
        if(mFormValidator.isValid()){
            mManageAccountPresenter.updateDataPassenger(mPrefsTaxsi.getData("id_pasajero"),
                    mNameEditText.getText().toString(),
                    mLastNameEditText.getText().toString(),
                    mAgeEditTex.getText().toString(),
                    mGenreEditText.getText().toString(),
                    mPhoneEditeText.getText().toString(),
                    mEmaiEditText.getText().toString(),
                    "");
        }
    }

    public void UpdatePasswordPassenger(){
        if(mFormValidator.isValid()){
            mManageAccountPresenter.updatePassword(mPrefsTaxsi.getData("id_pasajero"),
                    mPasswordOldEditText.getText().toString(),
                    mPasswordActiveEditText.getText().toString()
                    );
        }
    }

    private void setDataPassenger(){
        mNameDriverTextView.setText(mPrefsTaxsi.getData("nombre"));
        mPhoneTextView.setText(mPrefsTaxsi.getData("telefono"));
        mEmailDriverTextView.setText(mPrefsTaxsi.getData("email"));
        mNameEditText.setText(mPrefsTaxsi.getData("nombre"));
        mLastNameEditText.setText(mPrefsTaxsi.getData("apellidos"));
        mGenreEditText.setText(mPrefsTaxsi.getData("sexo"));
        mAgeEditTex.setText(mPrefsTaxsi.getData("edad"));
        mEmaiEditText.setText(mPrefsTaxsi.getData("email"));
        mPhoneEditeText.setText(mPrefsTaxsi.getData("telefono"));
    }

    public void setToolBar(){
        setTitle("Configuración de la cuenta");
        mBackImageView.setImageResource(R.drawable.ic_back_icon);
        mRichIconImageView.setVisibility(View.INVISIBLE);
    }

    public void setTitle(String mTitle){
        mTitleToolBarTextField.setText(mTitle);
    }

    @OnClick(R.id.mBackImageView)
    public void OnClickBack(){
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
