package com.sinergiadigital.taxsi.pasajero.Activities;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sinergiadigital.taxsi.pasajero.Adapters.HistoryAdapter;
import com.sinergiadigital.taxsi.pasajero.BaseClasses.BaseActivity;
import com.sinergiadigital.taxsi.pasajero.BaseClasses.BasePresenter;
import com.sinergiadigital.taxsi.pasajero.Implementation.HistoryPresenter;
import com.sinergiadigital.taxsi.pasajero.InternalData.PrefsTaxsi;
import com.sinergiadigital.taxsi.pasajero.Model.Viaje;
import com.sinergiadigital.taxsi.pasajero.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HistoryTripActivity extends BaseActivity implements HistoryPresenter.HistoryCallback{
    @BindView(R.id.mHistoryListRecyclerView)
    public RecyclerView mHistoryListRecyclerView;
    @BindView(R.id.mLeftMenuLinearLayout)
    public LinearLayout mLeftMenuLinearLayout;
    @BindView(R.id.mTitleToolBarTextField)
    public TextView mTitleToolBarTextField;
    @BindView(R.id.mBackImageView)
    public ImageView mBackImageView;
    @BindView(R.id.mRichIconImageView)
    public ImageView mRichIconImageView;
    @BindView(R.id.mRightMenuLinearLayout)
    public LinearLayout mRightMenuLinearLayout;
    private HistoryPresenter mHistoryPresenter;
    private HistoryAdapter mHistoryAdapter;
    private PrefsTaxsi mPrefsTaxsi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.history_activity);
        ButterKnife.bind(this);
        mPrefsTaxsi = new PrefsTaxsi(this);
        mHistoryListRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mHistoryPresenter.getHistorial(mPrefsTaxsi.getData("id_pasajero"),"");
        setTitle("Historial");
        setToolBar();
    }


    @Override
    public BasePresenter getPresenter() {
        mHistoryPresenter = new HistoryPresenter(this,this);
        return mHistoryPresenter;
    }

    public void setToolBar(){
        setTitle("Historial");
        mBackImageView.setImageResource(R.drawable.ic_back_icon);
        mRichIconImageView.setVisibility(View.INVISIBLE);
    }

    public void setTitle(String mTitle){
        mTitleToolBarTextField.setText(mTitle);
    }

    @OnClick(R.id.mBackImageView)
    public void OnClickBack(){
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void OnSuccessHistory(List<Viaje> listViaje) {
        mHistoryAdapter = new HistoryAdapter(this, listViaje);
        mHistoryListRecyclerView.setAdapter(mHistoryAdapter);
    }

}
