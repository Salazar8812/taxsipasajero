package com.sinergiadigital.taxsi.pasajero.Background.Request;

import com.google.gson.annotations.SerializedName;
import com.sinergiadigital.taxsi.pasajero.Background.WSBaseRequestInterface;

public class EvaluateRequest implements WSBaseRequestInterface {
    @SerializedName("id_conductor")
    public String id_pasajero;

    @SerializedName("id_viaje")
    public String id_viaje;

    @SerializedName("valoracion")
    public String valoracion;

    @SerializedName("comentario")
    public String comentario;

    public EvaluateRequest(String id_pasajero, String id_viaje, String valoracion, String comentario) {
        this.id_pasajero = id_pasajero;
        this.id_viaje = id_viaje;
        this.valoracion = valoracion;
        this.comentario = comentario;
    }
}
