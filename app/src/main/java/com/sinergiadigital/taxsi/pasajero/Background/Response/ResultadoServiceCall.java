package com.sinergiadigital.taxsi.pasajero.Background.Response;

import com.google.gson.annotations.SerializedName;

public class ResultadoServiceCall {
    @SerializedName("resultadoServiceCall")
    public String resultado;

    @SerializedName("id_viaje")
    public String id_viaje;

    @SerializedName("id_pasajero")
    public String id_pasajero;

    @SerializedName("id_conductor")
    public String id_conductor;

    @SerializedName("fecha_solicitud")
    public String fecha_solicitud;

    @SerializedName("lugar_partida")
    public String lugar_partida;

    @SerializedName("lugar_llegada")
    public String lugar_llegada;

    @SerializedName("latitud_partida")
    public String latitud_partida;

    @SerializedName("longitud_partida")
    public String longitud_partida;

    @SerializedName("latitud_llegada_sol")
    public String latitud_llegada_sol;

    @SerializedName("longitud_llegada_sol")
    public String longitud_llegada_sol;

    @SerializedName("estatus")
    public String estatus;
}
