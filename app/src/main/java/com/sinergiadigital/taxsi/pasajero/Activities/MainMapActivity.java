package com.sinergiadigital.taxsi.pasajero.Activities;

import android.content.Intent;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.sinergiadigital.taxsi.pasajero.BaseClasses.BaseActivity;
import com.sinergiadigital.taxsi.pasajero.GPSService.LocationRetrieve;
import com.sinergiadigital.taxsi.pasajero.InternalData.PrefsTaxsi;
import com.sinergiadigital.taxsi.pasajero.MapFragment;
import com.sinergiadigital.taxsi.pasajero.R;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import java.util.Timer;
import java.util.TimerTask;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainMapActivity extends BaseActivity{

    @BindView(R.id.mContainerFragment)
    public FrameLayout mContainerFragment;

    @BindView(R.id.mTitleToolBarTextField)
    public TextView mTitleToolBarTextField;
    @BindView(R.id.mBackImageView)
    public ImageView mBackImageView;

    private PrefsTaxsi mPrefsTaxsi;
    private String mLatitudPartida = "";
    private String mLongitudPartida = "";
    private String mLatitudLlegada = "";
    private String getmLongitudLlegada = "";
    private Timer timer;
    private TimerTask timerTask;
    private Location evenlocation;

    public GoogleMap map;
    private MapFragment mFragment;
    private FragmentManager FM;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_activity);
        ButterKnife.bind(this);
        mPrefsTaxsi = new PrefsTaxsi(this);

        mFragment = new MapFragment();
        loadFragment(mFragment);
    }

    @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB_MR2)
    public void loadFragment(Fragment fragment) {
        mContainerFragment.removeAllViews();
        FM = getSupportFragmentManager();
        FragmentTransaction FT = FM.beginTransaction();
        FT.replace(R.id.mContainerFragment, fragment);
        FT.commit();
    }

    @OnClick(R.id.mBackImageView)
    public void OnClickMenuOptionButton(){
        startActivity(new Intent(this, MainMenuActivity.class));
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void OnLocationRetrieve(final LocationRetrieve eventLocation) {
        String latitude = String.valueOf(eventLocation.location.getLatitude());
        String longitude = String.valueOf(eventLocation.location.getLongitude());
        this.evenlocation = eventLocation.getLocation();
        mPrefsTaxsi.saveData("latitude",latitude);
        mPrefsTaxsi.saveData("longitude",longitude);
    }

}
