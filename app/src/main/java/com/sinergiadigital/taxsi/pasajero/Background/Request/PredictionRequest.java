package com.sinergiadigital.taxsi.pasajero.Background.Request;

import com.google.gson.annotations.SerializedName;
import com.sinergiadigital.taxsi.pasajero.Background.WSBaseRequestInterface;

public class PredictionRequest implements WSBaseRequestInterface {
    public String mInput;
    public String mKey;

    public PredictionRequest(String mInput, String mKey) {
        this.mInput = mInput;
        this.mKey = mKey;
    }
}
