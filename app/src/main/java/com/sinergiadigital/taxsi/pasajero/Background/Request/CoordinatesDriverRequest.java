package com.sinergiadigital.taxsi.pasajero.Background.Request;

import com.sinergiadigital.taxsi.pasajero.Background.WSBaseRequestInterface;

public class CoordinatesDriverRequest implements WSBaseRequestInterface {
    public String id_viaje;

    public CoordinatesDriverRequest(String id_viaje) {
        this.id_viaje = id_viaje;
    }
}
