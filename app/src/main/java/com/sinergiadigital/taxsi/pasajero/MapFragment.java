package com.sinergiadigital.taxsi.pasajero;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.akexorcist.googledirection.DirectionCallback;
import com.akexorcist.googledirection.GoogleDirection;
import com.akexorcist.googledirection.constant.RequestResult;
import com.akexorcist.googledirection.constant.TransportMode;
import com.akexorcist.googledirection.model.Direction;
import com.akexorcist.googledirection.model.Info;
import com.akexorcist.googledirection.model.Leg;
import com.akexorcist.googledirection.model.Route;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import com.sinergiadigital.taxsi.pasajero.Background.Response.CalculatePriceResponse;
import com.sinergiadigital.taxsi.pasajero.BaseClasses.BaseFragment;
import com.sinergiadigital.taxsi.pasajero.BaseClasses.BasePresenter;
import com.sinergiadigital.taxsi.pasajero.GPSService.GPSClass;
import com.sinergiadigital.taxsi.pasajero.Implementation.CallServiceTravelPresenter;
import com.sinergiadigital.taxsi.pasajero.InternalData.PrefsTaxsi;
import com.sinergiadigital.taxsi.pasajero.Utils.DrawMarker;
import com.sinergiadigital.taxsi.pasajero.Utils.DrawRouteMaps;

import java.util.List;
import java.util.Locale;


public class MapFragment extends BaseFragment implements OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener,View.OnClickListener,CallServiceTravelPresenter.ServiceTaxsiCallback, GoogleApiClient.OnConnectionFailedListener {
    public TextView mOriginEditTextView;
    public TextView mArriveEditTextView;
    public TextView mCostoTarifaTextView;
    public GoogleMap map;
    private PrefsTaxsi mPrefsTaxsi;
    public Marker mk = null;
    private LocationManager locationManager;
    private boolean firstMoveDraw = true;
    private ImageView mLocationImaeView;
    private Button mCallServiceButton;
    private CallServiceTravelPresenter mCallServiceTravelPresenter;
    private static String KEY_PRICE_TARIFA = "tarifa" ;
    private LinearLayout mContentPriceLinearLayout;
    int PLACE_PICKER_REQUEST = 1;

    private GoogleApiClient mGoogleApiClient;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.map_fragment, container, false);
        mPrefsTaxsi = new PrefsTaxsi(getActivity());
        bindViews(v);

        SupportMapFragment mapFrag = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFrag.getMapAsync(this);

        addOnClickListener();
        retrieveTarifa();

        mGoogleApiClient = new GoogleApiClient
                .Builder(getActivity())
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .enableAutoManage(getActivity(), this)
                .build();

        return v;
    }


    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    public void bindViews(View v){
        mOriginEditTextView = v.findViewById(R.id.mOriginTextView);
        mArriveEditTextView = v.findViewById(R.id.mArriveTextView);
        mLocationImaeView = v.findViewById(R.id.mLocationImaeView);
        mCallServiceButton = v.findViewById(R.id.mCallServiceButton);
        mCostoTarifaTextView = v.findViewById(R.id.mCostoTarifaTextView);
        mContentPriceLinearLayout = v.findViewById(R.id.mContentPriceLinearLayout);
    }

    public void retrieveTarifa(){
        String tarifa = getTarifa();
        if(tarifa != null) {
            if (!tarifa.equals("") || !tarifa.isEmpty()) {
                mContentPriceLinearLayout.setVisibility(View.VISIBLE);
                mCostoTarifaTextView.setText("El costo aproximado de la tarifa es: $" + getTarifa() + " pesos");
            }
        }
    }

    private void addOnClickListener(){
        mLocationImaeView.setOnClickListener(this);
        mCallServiceButton.setOnClickListener(this);
        mOriginEditTextView.setOnClickListener(this);
        mArriveEditTextView.setOnClickListener(this);
    }


    @Override
    public BasePresenter[] getPresenters() {
        mCallServiceTravelPresenter = new CallServiceTravelPresenter((AppCompatActivity) getActivity(),this);
        return new BasePresenter[]{mCallServiceTravelPresenter};
    }

    @Override
    public void onInfoWindowClick(Marker marker) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        Location drawPoint = new Location("");
        try {
            drawPoint.setLatitude(Double.parseDouble(mPrefsTaxsi.getData("latitude")));
            drawPoint.setLongitude(Double.parseDouble(mPrefsTaxsi.getData("longitude")));
            createMarker(mPrefsTaxsi.getData("latitude"),mPrefsTaxsi.getData("longitude"));
        }catch (NumberFormatException e){
            e.printStackTrace();
            gpsCheckStatus();
            Location drawPoint2 = new Location("");
            drawPoint2.setLatitude(0.0);
            drawPoint2.setLongitude(0.0);
            createMarker("0.0","0.0");
        }

    }

    public void drawOrigin(String mLatitud, String mLongitud){
        createMarker(mLatitud,mLongitud);
    }

    public void drawRoute(LatLng mFrom, LatLng mArrive){
        try {
            LatLng origin = mFrom;
            LatLng destination = new LatLng(mArrive.latitude, mArrive.longitude);

            //if(firstMoveDraw) {
                map.clear();
                DrawRouteMaps.getInstance(getActivity())
                        .draw(origin, destination, map);

                //Location prevLoc = mOrigin;
                //Location newLoc = prevLoc ;
                // float bearing = prevLoc.bearingTo(newLoc) ;

                DrawMarker.getInstance(getActivity()).drawOrigin(map,mFrom,destination,R.mipmap.ic_car_icon_t, "Mi Ubicación"/*,bearing*/);
                DrawMarker.getInstance(getActivity()).drawDestination(map, destination, R.mipmap.ic_marker_map, "Destino");

                LatLngBounds bounds = new LatLngBounds.Builder()
                        .include(origin)
                        .include(destination).build();
                Point displaySize = new Point();
                getActivity().getWindowManager().getDefaultDisplay().getSize(displaySize);

                firstMoveDraw = false;
                map.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, displaySize.x, 250, 30));
            //}else{
           //     DrawMarker.getInstance(getActivity()).drawOrigin(map,mFrom,destination,R.mipmap.ic_car_icon_t, "Mi Ubicación"/*,bearing*/);
            //}
        } catch (NullPointerException e){
            e.printStackTrace();
        } catch (RuntimeException e){
            e.printStackTrace();
        }
    }

    public void cleanMap(){
        map.clear();
    }

    public void createMarker(String mLatitud, String mLongitud){
        map.clear();
        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        try {
            int height = 120;
            int width = 120;
            BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.mipmap.ic_car_icon_t);
            Bitmap b = bitmapdraw.getBitmap();
            Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);

            mk = map.addMarker(new MarkerOptions()
                    .position(new LatLng(Double.parseDouble(mLatitud),Double.parseDouble(mLongitud)))
                    .title("")
                    .flat(true)
                    .icon(BitmapDescriptorFactory.fromBitmap((smallMarker))));

            map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(mLatitud),Double.parseDouble(mLongitud)), 16.5f));

        }catch (NullPointerException | NumberFormatException e){
            e.printStackTrace();
        }
    }

    @Override
    public void OnSuccessGetTarifa(CalculatePriceResponse calculatePriceResponse) {
        saveTarifa(calculatePriceResponse.tarifa.get(0).tarifa);
        mContentPriceLinearLayout.setVisibility(View.VISIBLE);
        mCostoTarifaTextView.setText("El costo aproximado de la tarifa es: $"+ calculatePriceResponse.tarifa.get(0).tarifa+" pesos");
    }

    @Override
    public void OnSuccessCallServiceTaxsi() {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.mLocationImaeView:
                try {
                    int height = 120;
                    int width = 120;
                    BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.mipmap.ic_car_icon_t);
                    Bitmap b = bitmapdraw.getBitmap();
                    Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);

                    LatLng driverLocation = new LatLng(Double.parseDouble(mPrefsTaxsi.getData("latitude")), Double.parseDouble(mPrefsTaxsi.getData("longitude")));
                    Marker marcador = map.addMarker(new MarkerOptions()
                            .position(driverLocation)
                            .title("")
                            .icon(BitmapDescriptorFactory.fromBitmap((smallMarker))));

                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(driverLocation, 16.5f));
                }catch (NullPointerException | NumberFormatException e){
                    e.printStackTrace();
                }
                break;

            case R.id.mCallServiceButton:
                //mCallServiceTravelPresenter.callServiceTasxi(mLugarOrigen,mLugarLlegada,mLatOrigin,mLongOrigin,mLatDestiny,mLongDestiny);
                break;

            case R.id.mOriginTextView:
                /*Places.initialize(getActivity(), "AIzaSyCgyctHGlCEinu9zGEd2aIx4BbVorpMaUI");
                PlacesClient placesClient = Places.createClient(getActivity());*/

                startAutocompleteActivity();
                break;
            case R.id.mArriveTextView:

                break;
        }
    }

    private void startAutocompleteActivity() {
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            startActivityForResult(builder.build(getActivity()), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }


    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        /*if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, getActivity());
                String toastMsg = String.format("Place: %s", place.getName());
                Toast.makeText(getActivity(), toastMsg, Toast.LENGTH_LONG).show();
            }
        }*/
    }

    private void getDestinationInfo(String latitudPartida, String longitudPartida, String latitudLlegada, String longitudLlegada) {
        String serverKey = "AIzaSyABwUw-_wdaAyN1_aB0iLN6wtM86JouFWc"; // Api Key For Google Direction API \\
        final LatLng origin = new LatLng(Double.parseDouble(latitudPartida), Double.parseDouble(longitudPartida));
        final LatLng destination = new LatLng(Double.parseDouble(latitudLlegada), Double.parseDouble(longitudLlegada));

        GoogleDirection.withServerKey(serverKey)
                .from(origin)
                .to(destination)
                .transportMode(TransportMode.DRIVING)
                .execute(new DirectionCallback() {
                    @Override
                    public void onDirectionSuccess(Direction direction, String rawBody) {
                        String status = direction.getStatus();
                        if (status.equals(RequestResult.OK)) {
                            Route route = direction.getRouteList().get(0);
                            Leg leg = route.getLegList().get(0);
                            Info distanceInfo = leg.getDistance();
                            Info durationInfo = leg.getDuration();
                            String distance = distanceInfo.getText();
                            String duration = durationInfo.getText();

                            mCallServiceTravelPresenter.calculatePrice(distance,duration);

                        } else if (status.equals(RequestResult.NOT_FOUND)) {
                            Log.e("Error","No existe ruta");
                        }
                    }

                    @Override
                    public void onDirectionFailure(Throwable t) {
                        // Do something here
                    }
                });
    }

    private void initServiceGPS(){
        Intent i =new Intent(getActivity().getApplicationContext(),GPSClass.class);
        getActivity().startService(i);
    }

    public void gpsCheckStatus(){
        locationManager = (LocationManager) getActivity().getSystemService(getActivity().LOCATION_SERVICE);
        boolean enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (!enabled) {
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivityForResult(intent,0);
        }else{
            starLocation();
        }
    }

    public void starLocation(){
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
                new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        String latitude = intent.getStringExtra(GPSClass.EXTRA_LATITUDE);
                        String longitude = intent.getStringExtra(GPSClass.EXTRA_LONGITUDE);
                        Log.e("Latitude", latitude);
                        Log.e("Longitud", longitude);
                        mPrefsTaxsi.saveData("latitude",latitude);
                        mPrefsTaxsi.saveData("longitude",longitude);
                    }
                }, new IntentFilter(GPSClass.ACTION_LOCATION_BROADCAST)
        );

        initServiceGPS();
    }

    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
                Log.w("My Current", strReturnedAddress.toString());
            } else {
                Log.w("My Current", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("My Current", "Canont get Address!");
        }
        return strAdd;
    }

    public void setfirstMoveDraw(){
        firstMoveDraw = true;
    }

    public void saveTarifa(String valueTarife){
        mPrefsTaxsi.saveData(KEY_PRICE_TARIFA, valueTarife);
    }

    public String getTarifa(){
        return mPrefsTaxsi.getData(KEY_PRICE_TARIFA);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e("Error",connectionResult.getErrorMessage());
    }
}

