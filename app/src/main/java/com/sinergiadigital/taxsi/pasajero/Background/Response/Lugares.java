package com.sinergiadigital.taxsi.pasajero.Background.Response;

import com.google.gson.annotations.SerializedName;

public class Lugares{
    @SerializedName("latitud")
    public String latitud;

    @SerializedName("longitud")
    public String longitud;

    @SerializedName("id")
    public String id;

    @SerializedName("nombre_lugar")
    public String nombre_lugar;
}
