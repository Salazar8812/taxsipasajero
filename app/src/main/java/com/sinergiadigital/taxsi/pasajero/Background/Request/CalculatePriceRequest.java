package com.sinergiadigital.taxsi.pasajero.Background.Request;

import com.sinergiadigital.taxsi.pasajero.Background.WSBaseRequestInterface;

import java.io.Serializable;

public class CalculatePriceRequest implements Serializable,WSBaseRequestInterface {
    public String distancia;
    public String tiempo;

    public CalculatePriceRequest(String distancia, String tiempo) {
        this.distancia = distancia;
        this.tiempo = tiempo;
    }

}
