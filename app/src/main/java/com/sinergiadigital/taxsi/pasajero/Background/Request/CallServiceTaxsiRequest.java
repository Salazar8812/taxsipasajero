package com.sinergiadigital.taxsi.pasajero.Background.Request;

import com.sinergiadigital.taxsi.pasajero.Background.WSBaseRequestInterface;

public class CallServiceTaxsiRequest implements WSBaseRequestInterface {
    public String id_pasassenger;
    public String lugar_partida;
    public String lugar_llegada;
    public String latitud_partida;
    public String longitud_partida;
    public String latitud_llegada_sol;
    public String longitud_llegada_sol;

    public CallServiceTaxsiRequest(String id_pasassenger, String lugar_partida, String lugar_llegada, String latitud_partida, String longitud_partida, String latitud_llegada_sol, String longitud_llegada_sol) {
        this.id_pasassenger = id_pasassenger;
        this.lugar_partida = lugar_partida;
        this.lugar_llegada = lugar_llegada;
        this.latitud_partida = latitud_partida;
        this.longitud_partida = longitud_partida;
        this.latitud_llegada_sol = latitud_llegada_sol;
        this.longitud_llegada_sol = longitud_llegada_sol;
    }
}
