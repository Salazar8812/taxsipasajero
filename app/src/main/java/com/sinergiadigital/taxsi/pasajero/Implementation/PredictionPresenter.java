package com.sinergiadigital.taxsi.pasajero.Implementation;

import android.support.v7.app.AppCompatActivity;

import com.sinergiadigital.taxsi.pasajero.Background.BaseWSManager;
import com.sinergiadigital.taxsi.pasajero.Background.Request.PredictionRequest;
import com.sinergiadigital.taxsi.pasajero.Background.Response.DetailPlaceResponse;
import com.sinergiadigital.taxsi.pasajero.Background.Response.PredictionsResponse;
import com.sinergiadigital.taxsi.pasajero.Background.WSBaseResponseInterface;
import com.sinergiadigital.taxsi.pasajero.Background.WSManager;
import com.sinergiadigital.taxsi.pasajero.Utils.MessageUtils;

public class PredictionPresenter extends TaxsiPresenter {
    private String mKeyGoogle = "AIzaSyCgcbDXiHOSFLAGDGS_b3uwQRP6RfGLQtg";
    private AutocompleteCallback mAutocompleteCallback;
    private String mTypeSearch;

    public interface AutocompleteCallback{
        void OnSuccessAutocomplete(PredictionsResponse mPredictionsResponse, String mTypeSearch);

        void OnSuccessDetailPlace(DetailPlaceResponse mDetailPlaceResponse, String mTypeSearch);
    }

    public PredictionPresenter(AppCompatActivity appCompatActivity, AutocompleteCallback mAutocompleteCallback) {
        super(appCompatActivity);
        this.mAutocompleteCallback = mAutocompleteCallback;
    }

    @Override
    public BaseWSManager initWSManager() {
        return mWSManager = WSManager.init().settings(mContext, this);
    }

    @Override
    public void onRequestWS(String requestUrl) {
        super.onRequestWS(requestUrl);
        MessageUtils.stopProgress();
    }

    public void getPrediction(String mSearch, String mTypeSearch){
        this.mTypeSearch = mTypeSearch;
        PredictionRequest predictionRequest = new PredictionRequest(mSearch,mKeyGoogle);
        mWSManager.requestWs(PredictionsResponse.class, WSManager.WS.AUTOCOMPLETE, predictionRequest);
    }

    public void getLatLongDetail(String placeId, String mTypeSearch){
        this.mTypeSearch = mTypeSearch;
        PredictionRequest predictionRequest = new PredictionRequest(placeId,mKeyGoogle);
        mWSManager.requestWs(DetailPlaceResponse.class, WSManager.WS.GETLATLNG, predictionRequest);
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        switch (requestUrl){
            case WSManager.WS.AUTOCOMPLETE:
                OnSuccessAutoComplete((PredictionsResponse) baseResponse);
                break;

            case WSManager.WS.GETLATLNG:
                OnSuccessGetDetailLatLng((DetailPlaceResponse) baseResponse);
                break;
        }
    }

    public void OnSuccessAutoComplete(PredictionsResponse mPredictionsResponse){
        if(mPredictionsResponse != null){
            if(mPredictionsResponse.predictions.size() > 0){
                mAutocompleteCallback.OnSuccessAutocomplete(mPredictionsResponse, mTypeSearch);
            }
        }
    }

    public void OnSuccessGetDetailLatLng(DetailPlaceResponse mDetailPlaceResponse){
        if(mDetailPlaceResponse != null){
            if(mDetailPlaceResponse.resultDetail.geometry.locationPlace != null){
                mAutocompleteCallback.OnSuccessDetailPlace(mDetailPlaceResponse,mTypeSearch);
            }
        }
    }
}
