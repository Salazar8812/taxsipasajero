package com.sinergiadigital.taxsi.pasajero.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sinergiadigital.taxsi.pasajero.BaseClasses.BaseActivity;
import com.sinergiadigital.taxsi.pasajero.InternalData.PrefsTaxsi;
import com.sinergiadigital.taxsi.pasajero.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainMenuActivity extends BaseActivity {
    private PrefsTaxsi mPrefsTaxsi;
    @BindView(R.id.mLeftMenuLinearLayout)
    public LinearLayout mLeftMenuLinearLayout;
    @BindView(R.id.mTitleToolBarTextField)
    public TextView mTitleToolBarTextField;
    @BindView(R.id.mBackImageView)
    public ImageView mBackImageView;
    @BindView(R.id.mRichIconImageView)
    public ImageView mRichIconImageView;
    @BindView(R.id.mRightMenuLinearLayout)
    public LinearLayout mRightMenuLinearLayout;

    @BindView(R.id.mProfileImageView)
    ImageView mProfileImageView;

    @BindView(R.id.mNameDriverTextView)
    TextView mNameDriverTextView;

    @BindView(R.id.mPhoneTextView)
    TextView mPhoneTextView;

    @BindView(R.id.mEmailDriverTextView)
    TextView mEmailDriverTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_menu_activity);
        ButterKnife.bind(this);
        mPrefsTaxsi = new PrefsTaxsi(this);
        setToolBar();
        setTitle("Perfil de usuario");

        setDataPassenger();
    }

    private void setDataPassenger(){
        mNameDriverTextView.setText(mPrefsTaxsi.getData("nombre"));
        mPhoneTextView.setText(mPrefsTaxsi.getData("telefono"));
        mEmailDriverTextView.setText(mPrefsTaxsi.getData("email"));
    }

    @OnClick({R.id.mHistoryTripTextView, R.id.mFavsOptionTextView, R.id.mManageAccountTextView, R.id.mCloseSessionTextView})
    public void OnSelectOption(View view){
        switch (view.getId()){
            case R.id.mHistoryTripTextView:
                startActivity(new Intent(this, HistoryTripActivity.class));
                break;
            case R.id.mFavsOptionTextView:
                startActivity(new Intent(this, FavsActivity.class));
                break;
            case R.id.mManageAccountTextView:
                startActivity(new Intent(this, ManageAccountActivity.class));
                break;
            case R.id.mCloseSessionTextView:
                clearData();
                Intent intent = new Intent(this,LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                break;
                default:break;
        }
    }

    public void setToolBar(){
        setTitle("Historial");
        mBackImageView.setImageResource(R.drawable.ic_back_icon);
        mRichIconImageView.setVisibility(View.INVISIBLE);
    }

    public void setTitle(String mTitle){
        mTitleToolBarTextField.setText(mTitle);
    }

    @OnClick(R.id.mBackImageView)
    public void OnClickBack(){
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    public void clearData(){
        mPrefsTaxsi.removeAllSharedPreference();
    }

}
