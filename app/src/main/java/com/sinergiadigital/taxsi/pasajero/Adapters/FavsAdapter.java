package com.sinergiadigital.taxsi.pasajero.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sinergiadigital.taxsi.pasajero.Background.Response.Lugares;
import com.sinergiadigital.taxsi.pasajero.R;

import java.util.ArrayList;
import java.util.List;

public class FavsAdapter extends RecyclerView.Adapter<FavsAdapter.FavsHolder>  {
    private List<Lugares> mListFavs = new ArrayList<>();

    @Override
    public FavsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_favs, parent, false);
        FavsAdapter.FavsHolder vHolder = new FavsAdapter.FavsHolder(layoutView);
        return vHolder;
    }

    @Override
    public void onBindViewHolder(FavsHolder holder, int position) {
        holder.mFavsTitle.setText(mListFavs.get(position).nombre_lugar);
    }

    public void update(List<Lugares> mItems){
        mListFavs = mItems;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mListFavs.size();
    }

    public class FavsHolder extends RecyclerView.ViewHolder {
        TextView mFavsTitle;


        public FavsHolder(View itemView) {
            super(itemView);
            mFavsTitle = itemView.findViewById(R.id.mNameFavsTextField);
        }

    }
}
