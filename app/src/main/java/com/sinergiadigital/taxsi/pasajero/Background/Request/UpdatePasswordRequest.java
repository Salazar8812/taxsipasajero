package com.sinergiadigital.taxsi.pasajero.Background.Request;

import com.sinergiadigital.taxsi.pasajero.Background.WSBaseRequestInterface;

public class UpdatePasswordRequest implements WSBaseRequestInterface {
    public String id_pasajero;
    public String viejo_pass;
    public String nuevo_pass;

    public UpdatePasswordRequest(String id_pasajero, String viejo_pass, String nuevo_pass) {
        this.id_pasajero = id_pasajero;
        this.viejo_pass = viejo_pass;
        this.nuevo_pass = nuevo_pass;
    }
}
