package com.sinergiadigital.taxsi.pasajero.Implementation;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import com.sinergiadigital.taxsi.pasajero.Background.BaseWSManager;
import com.sinergiadigital.taxsi.pasajero.Background.Request.LoginRequest;
import com.sinergiadigital.taxsi.pasajero.Background.Response.LoginResponse;
import com.sinergiadigital.taxsi.pasajero.Background.WSBaseResponseInterface;
import com.sinergiadigital.taxsi.pasajero.Background.WSManager;
import com.sinergiadigital.taxsi.pasajero.InternalData.PrefsTaxsi;
import com.sinergiadigital.taxsi.pasajero.Activities.MainMapActivity;
import com.sinergiadigital.taxsi.pasajero.Utils.MessageUtils;

public class LoginPresenter extends TaxsiPresenter{
    private PrefsTaxsi mPrefsTaxsi;
    private String email, password;

    public LoginPresenter(AppCompatActivity appCompatActivity) {
        super(appCompatActivity);
    }

    @Override
    public BaseWSManager initWSManager() {
        return mWSManager = WSManager.init().settings(mContext, this);
    }

    public void doLogin(String mEmail, String mPassword){
        mPrefsTaxsi = new PrefsTaxsi(mAppCompatActivity);
        email = mEmail;
        password = mPassword;
        LoginRequest loginRequest = new LoginRequest("2",mEmail,mPassword);
        mWSManager.requestWs(LoginResponse.class, WSManager.WS.LOGIN, loginRequest);
    }

    @Override
    public void onSuccessLoadResponse(String requestUrl, WSBaseResponseInterface baseResponse) {
        if(WSManager.WS.LOGIN.equals(requestUrl)){
            OnSuccessLogin((LoginResponse) baseResponse);
        }
    }

    public void OnSuccessLogin(LoginResponse loginResponse){
        MessageUtils.stopProgress();

        if(loginResponse != null){
            if(loginResponse.mPassenger.get(0).apellidos != null){
                saveDataPassenger(loginResponse);
                mAppCompatActivity.startActivity(new Intent(mAppCompatActivity,MainMapActivity.class));
            }
        }
    }

    private void saveDataPassenger(LoginResponse loginResponse){
        mPrefsTaxsi.saveData("id_pasajero",loginResponse.mPassenger.get(0).id);
        mPrefsTaxsi.saveData("nombre",loginResponse.mPassenger.get(0).nombre);
        mPrefsTaxsi.saveData("apellidos",loginResponse.mPassenger.get(0).apellidos);
        mPrefsTaxsi.saveData("email",loginResponse.mPassenger.get(0).email);
        mPrefsTaxsi.saveData("edad",loginResponse.mPassenger.get(0).edad);
        mPrefsTaxsi.saveData("telefono",loginResponse.mPassenger.get(0).telefono);
        mPrefsTaxsi.saveData("sexo",loginResponse.mPassenger.get(0).sexo);
    }
}
