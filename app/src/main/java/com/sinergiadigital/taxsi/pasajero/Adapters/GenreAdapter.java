package com.sinergiadigital.taxsi.pasajero.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sinergiadigital.taxsi.pasajero.R;

import java.util.ArrayList;
import java.util.List;



public class GenreAdapter extends RecyclerView.Adapter<GenreAdapter.GenereHolder>  {
    private List<String> mListFavs = new ArrayList<>();
    private GenreCallback mGenreCallback;

    public interface GenreCallback{
        void OnGetGenre(String mGenre);
    }

    public GenreAdapter(GenreCallback mGenreCallback) {
        this.mGenreCallback = mGenreCallback;
    }

    @Override
    public GenereHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_genre, parent, false);
        GenreAdapter.GenereHolder vHolder = new GenreAdapter.GenereHolder(layoutView);
        return vHolder;
    }

    @Override
    public void onBindViewHolder(GenereHolder holder, int position) {
        holder.mGenreOptionTitle.setText(mListFavs.get(position));
    }

    public void update(List<String> mItems){
        mListFavs = mItems;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mListFavs.size();
    }

    public class GenereHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView mGenreOptionTitle;

        public GenereHolder(View itemView) {
            super(itemView);
            mGenreOptionTitle = itemView.findViewById(R.id.genre_option_textView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            mGenreCallback.OnGetGenre(mGenreOptionTitle.getText().toString());
        }
    }
}
