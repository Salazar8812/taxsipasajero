package com.sinergiadigital.taxsi.pasajero.Background.Response;

import com.google.gson.annotations.SerializedName;
import com.sinergiadigital.taxsi.pasajero.Background.WSBaseResponseInterface;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CalculatePriceResponse implements Serializable,WSBaseResponseInterface {
    @SerializedName("tarifa")
    public List<Tarifa> tarifa = new ArrayList<>();


    public class Tarifa{
        @SerializedName("tarifa")
        public String tarifa;
    }
}
