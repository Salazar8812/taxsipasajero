/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.sinergiadigital.taxsi.pasajero;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.sinergiadigital.taxsi.pasajero";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.0";
  // Fields from build type: debug
  public static final String PRODUCTION_URL = "https://www.taxsi.mx/";
  public static final String PRODUCTION_URL_TOTALAPPS = "https://www.taxsi.mx/";
}
